<?php

require_once __DIR__ . '/inc/contactform3.php';

get_header( 'beotravel' );

$args = array(
    'post_type' => 'slider1',
);
$query = new WP_Query( $args );
if( $query->have_posts() ) : ?>
    <div id="main-carousel" class="carousel beogroup-carousel section slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <?php
            $counter = 0;
                while( $query->have_posts() ) :
                    $counter++;
                    $query->the_post(); ?>
                        <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                            <a href="<?php echo get_post_meta( get_the_ID(),'_contact_value_key12',true ); ?>"><?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?></a>
                            <div class="carousel-caption beogroup-carousel-caption">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>
            <?php endwhile;
            wp_reset_postdata();
            if ( $counter != 1 ) {
                ?>
                <ol class="carousel-indicators">
                <?php
                for( $i=0; $i<$counter; $i++ ) {
                    ?>
                    <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                    <?php
                } ?>
                </ol>
                <?php
            } ?>
        </div>
    </div>
<?php
else :
    _e( 'Sorry, no content found', 'beogroup' );
endif;

?>

<?php
$price = get_post_meta( get_the_ID(), '_contact_value_key1234', true );
$duration = get_post_meta( get_the_ID(), '_duration_value_key', true );
$call = get_post_meta( get_the_ID(), '_call_value_key', true );
$for_sale = get_post_meta( get_the_ID(), '_for_sale_value_key', true );
?>

<div class="singlec-cruise">
    <div class="container single-cruise-container">
        <h2><?php the_title(); ?></h2>
        <div class="col-xs-12- col-sm-6 single-cruise-text">
            <h3><?php the_excerpt(); ?></h3>
            <?php the_content(); ?>
            <div class="single-cruise-text-buttons">
                <a href="#">Enquiry</a>
                <div class="">
                    <form action="#" method="post">
                        <label for="name">Your Name</label>
                        <input id="name" type="text" name="name"> <br>
                        <label for="email">E-mail</label>
                        <input id="email" type="email" name="email"> <br>
                        <label for="subject">Subject</label>
                        <input id="subject" type="text" name="subject">
                        <label for="message">Your Message</label>
                        <textarea id="message" name="message" rows="8" cols="80"></textarea> <br>
                        <input type="hidden" name="cruise" value="<?php the_title(); ?>">
                        <input class="blue-button" type="submit" name="btn_submit3" value="Send">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12- col-sm-6 single-cruise-img">
            <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
        </div>
    </div>
</div>
<div class="single-cruise-info">
    <div class="single-cruise-info-container container">
        <div class="col-xs-12 headerino">
            <h4>Info</h4>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 infos">
            <p>Price: <span><?php echo $price; ?></span></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 infos">
            <p>Duration: <span><?php echo $duration; ?></span></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 infos">
            <p>Call su on: <span><?php echo $call; ?></span></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 infos">
            <p>Valid for sale: <span><?php echo $for_sale; ?></span></p>
        </div>
    </div>
</div>

<?php

get_footer();
