<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php require_once __DIR__ . '/inc/contactform2.php'; ?>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>

    <body <?php body_class(); ?>>
        <header class="site-header overseas-header">
            <nav class="container header-container navigation-bar navbar navbar-default">
                <?php
                $custom_logo = get_theme_mod( 'custom_footer_text3' );
                $image = wp_get_attachment_image_src( $custom_logo, 'full' );
                ?>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo home_url('/properties-overseas'); ?>">
                    <img src="<?php echo $custom_logo; ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>">
                </a>

                <div class="collapse navbar-collapse" id="primary-navigation">
                    <?php
                    $args = array(
                        'theme_location' => 'primary4',
                        'menu_class' => 'nav navbar-nav navbar-right list-items',
                    );
                    wp_nav_menu( $args );
                    ?>
                </div>
                <?php
                // $args2 = array(
                //     'theme_location' => 'languages',
                // );
                // wp_nav_menu( $args2 );
                ?>
            </nav>
        </header>
