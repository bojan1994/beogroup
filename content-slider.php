<?php

$args = array(
    'post_type' => 'slider',
    'posts_per_page' => 10,
);
$query = new WP_Query( $args );
if( $query->have_posts() ) : ?>
    <div id="main-carousel" class="carousel beogroup-carousel section slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <?php
            $counter = 0;
                while( $query->have_posts() ) :
                    $counter++;
                    $query->the_post(); ?>
                        <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                            <a href="<?php echo get_post_meta( get_the_ID(),'_contact_value_key12',true ); ?>"><?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?></a>
                            <div class="carousel-caption beogroup-carousel-caption">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>
            <?php endwhile;
            wp_reset_postdata();
            if ( $counter != 1 ) {
                ?>
                <ol class="carousel-indicators">
                <?php
                for( $i=0; $i<$counter; $i++ ) {
                    ?>
                    <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                    <?php
                } ?>
                </ol>
                <?php
            } ?>
        </div>
    </div>
<?php
else :
    _e( 'Sorry, no content found', 'beogroup' );
endif;
