<?php

if( is_page( 576 ) || is_page( 578 ) || is_page( 580 ) || is_page( 582 ) || is_page( 749 ) ) {
    get_header( 'beoexport' );
} elseif( is_page( 181 ) || is_page( 187 ) || is_page( 192 ) || is_page( 184 ) || is_page( 533 ) ) {
    get_header( 'beotravel' );
} elseif( is_page( 221 ) || is_page( 223 ) || is_page( 227 ) || is_page( 529 ) ) {
    get_header( 'bendigo-bank' );
} elseif( is_page( 296 ) || is_page( 300 ) || is_page( 302 ) || is_page( 304 ) || is_page( 535 ) ) {
    get_header( 'properties-overseas' );
} elseif( is_page( 329 ) || is_page( 531 ) ) {
    get_header( 'beoradio' );
} elseif( is_page( 1382 ) || is_page( 1384 ) || is_page( 1386 ) || is_page( 1388 ) || is_page( 1390 ) ) {
    get_header( 'beomedia' );
} else {
    get_header();
}

if( have_posts() ) :
    while( have_posts() ) :
        the_post();
        the_content();
    endwhile;
else :
    _e( 'Sorry, no content found', 'beogroup' );
endif;

get_footer();
