var $ = jQuery.noConflict();

$(function() {
	$('.individual, .references-image, .matched, .single-event, .offer, .individual, .bendigo-team-single, .check-column, .ovo, .ono, .disco, .cruise-single, .gallery-single').matchHeight();
	$('.sites').matchHeight();
    $('.footero').matchHeight();
	$('.distribution').matchHeight();
});

$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
});

$(document).ready(function() {

	$('.single-cruise-text-buttons > a').click(function(e) {
		e.preventDefault();
		if ($('.single-cruise-text-buttons form').css('display') == 'none') {
			$('.single-cruise-text-buttons form').slideDown();
		}
		else {
			$('.single-cruise-text-buttons form').slideUp();
		}
	});

	console.log($('.site-header').height());

	$(window).on("resize", function() {
		$(".carousel").css("margin-top", ($('.site-header').height() - 1) + "px");
	});

	$(window).on("load", function() {
		$(".carousel").css("margin-top", ($('.site-header').height() -1) + "px");
	});


	$('.slick-carousel').slick({
	    arrows: false,
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 0,
		cssEase: 'linear',
		infinite: true,
		pauseOnFocus: false,
		pauseOnHover: false,
		speed: 50000,
		waitForAnimate: false
  	});

	var o = $('.real-estate-single-img a img').attr('src');
	console.log(o);

    var trigger = $('.question');
    $(trigger).each(function() {
        $(this).click(function() {
            var x = $(this).parent().find('.answer');
            if ($(x).css('display') == 'none') {
				x.slideDown();
			}
			else {
				x.slideUp();
			}
        });
    });

	$('.carousel').carousel({
		interval: 7000
	})

    var x = $('#primary-navigation a').last();
    //console.log(x);
    x.attr("class","pre");

    var y = $('.menu-languages-container a');
    //console.log(y);
    y.each(function() {
        $(this).css('padding-top, 0');
    });

	$('.footer-links div p').each(function(){
		$(this).replaceWith('<a href="#">' + $(this).text() + '</a>');
	});

	var beoExport = $('.footer-links div > a:contains("BEO-Export")');
	beoExport.attr('href', 'https://dev1.m1.rs/beogroup/beoexport/');

	var beoTravel = $('.footer-links div > a:contains("BEO-Travel")');
	beoTravel.attr('href', 'https://dev1.m1.rs/beogroup/beotravel/');

	var bendigoBank = $('.footer-links div > a:contains("Bendigo Bank")');
	bendigoBank.attr('href', 'https://dev1.m1.rs/beogroup/bendigo-bank/');

	var proOver = $('.footer-links div > a:contains("Real Estate Overseas")');
	proOver.attr('href', 'https://dev1.m1.rs/beogroup/properties-overseas/');

	var beoRadio = $('.footer-links div > a:contains("BEO-Radio")');
	beoRadio.attr('href', 'https://dev1.m1.rs/beogroup/beo-radio/');

	// direct browser to top right away
	if (window.location.hash)
	    scroll(0,0);
	// takes care of some browsers issue
	setTimeout(function(){scroll(0,0);},1);

	$(function(){
	//your current click function
	$('.scroll').on('click',function(e){
	    e.preventDefault();
	    $('html,body').animate({
	        scrollTop:$($(this).attr('href')).offset().top + 'px'
	    },1000,'swing');
	});

	// if we have anchor on the url (calling from other page)
	if(window.location.hash){
	    // smooth scroll to the anchor id
	    $('html,body').animate({
	        scrollTop:$(window.location.hash).offset().top + 'px'
	        },1000,'swing');
	    }
	});

	$('.pre').prepend('<i class="fas fa-phone"></i>');

	$('.send-money-blue-container p').prepend('<i class="fas fa-check"></i>');

});

$(document).ready(function () {

    $('#iznos_uplate,#iznos_isplate').keypress(function(event){
        //console.log(event.which);
    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault();
        alert("Allowed only numbers! ")
    }});
});

jQuery(function($) {
    $('#iznos_uplate').on('keyup', function() {
        if($('#iznos_uplate').val() == '' && $('#iznos_isplate').val() == ''){
        } else {
            izracunajProviziju();
        }
    });
    $('#iznos_uplate').on('keydown', function() {
        $('#iznos_isplate').val('');
    });
    $('#iznos_isplate').on('keyup', function() {
        if($('#iznos_uplate').val() == '' && $('#iznos_isplate').val() == ''){
        } else {
            izracunajProviziju();
        }
    });
    $('#iznos_isplate').on('keydown', function() {
        $('#iznos_uplate').val('');
    });
});

function izracunajProviziju() {
    var iznos_uplate = $("#iznos_uplate").val();
    var odnos_valuta = $("#odnos_valuta").val();
    var iznos_isplate = $("#iznos_isplate").val();
    if(iznos_uplate == 0 && iznos_isplate == 0){
        alert("Unesite iznos Uplate ili Isplate!");
        $('#iznos_uplate').focus();
        return;
    }
    odnos_valuta = odnos_valuta.substring(0, odnos_valuta.indexOf("|"));
    if($("#iznos_uplate").val() == ''){
        $('#iznos_uplate').val((iznos_isplate / odnos_valuta).toFixed(2));
    } else {
        $ ('#iznos_isplate').val((iznos_uplate * odnos_valuta).toFixed(2));
    }
}

document.getElementById("iznos_uplate").onkeyup=function(){
    var input=parseInt(this.value);
    if(input<0 || input>5000.00) {
        alert("To send over 5,000.00 AUD please visit Money transfers over 5,000");
        $("#iznos_uplate").val('');
    }
}

document.getElementById("iznos_isplate").onkeyup=function(){
    var input=parseInt(this.value);
    if(input<0 || input>5000.00) {
        alert("To send over 5,000.00 AUD please visit Money transfers over 5,000");
        $("#iznos_isplate").val('');
    }
}
