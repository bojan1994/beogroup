<?php

/* Register scripts */
if( ! function_exists( 'register_scripts' ) ) {
    function register_scripts() {
        wp_enqueue_style( 'fontawesome.min', get_template_directory_uri() . '/assets/css/fontawesome.min.css' );
        wp_enqueue_style( 'fontawesome-all.min', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css' );
        wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
        wp_enqueue_style( 'owl.carousel', get_template_directory_uri() . '/assets/css/slick.css' );
        wp_enqueue_style( 'style', get_stylesheet_uri(), array( 'owl.carousel' ) );
        wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/assets/js/matchHeight-min.js', array( 'bootstrap' ) );
        wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/js/smoothScroll.js', array( 'matchHeight' ) );
        wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'smoothscroll' ) );
        wp_enqueue_script( 'moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js' );
        wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'moment-js' ) );
    }
    add_action( 'wp_enqueue_scripts', 'register_scripts' );
}

/* Register Navigation Menus */
register_nav_menus( array(
    'primary' => __( 'BeoGroup Primary Navigation' ),
    'primary1' => __( 'BeoTravel Primary Navigation' ),
    'primary2' => __( 'Bendigo Bank Primary Navigation' ),
    'primary4' => __( 'Properties Overseas Primary Navigation' ),
    'primary6' => __( 'Beo Radio Primary Navigation' ),
    'primary5' => __( 'Beo Export Primary Navigation' ),
    'primary7' => __( 'Beo Media Primary Navigation' ),
    'footer' => __( 'Footer Navigation' ),
    'social' => __( 'Social networks' ),
    'languages' => __( 'Languages' ),
) );

/* Theme Setup */
if( ! function_exists( 'theme_setup' ) ) {
    function theme_setup() {
        $array = array(
            'height'      => 70,
            'width'       => 230,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        );
        add_theme_support( 'custom-logo', $array );
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'slider-image', 1920, 670 );
        add_image_size( 'slider-image2', 1920, 200 );
        add_image_size( 'services-min', 250, 250 );
        add_image_size( 'services-min2', 75, 75 );
        add_image_size( 'bgimage', 1920, 700 );
        add_image_size( 'home-image', 550, 650 );
        add_image_size( 'services', 40, 40 );
        add_image_size( 'signature', 140, 80 );
        add_image_size( 'current-offer', 300, 420 );
        add_image_size( 'gallery', 300, 300 );
        add_image_size( 'services3', 45, 45 );
        add_image_size( 'beotravel-team', 290, 340 );
        add_image_size( 'bgimage2', 1920, 380 );
        add_image_size( 'beotravel-references', 280, 50 );
        add_image_size( 'rent-image', 300, 220 );
        add_image_size( 'beoexport-services', 400, 260 );
        add_image_size( 'beoexport-team', 310, 350 );
        add_image_size( 'real-estate', 600, 500 );
        add_image_size( 'overseas-representative', 400, 500 );
        add_image_size( 'real-estate-search', 370, 300 );
        add_image_size( 'real-estate-current-offer', 900, 1200 );
        add_image_size( 'beomedia-home', 430, 300 );
        add_theme_support( 'title-tag' );
    }
    add_action( 'after_setup_theme', 'theme_setup' );
}

/* Custom footer */
if( ! function_exists( 'theme_customizer' ) ) {
    function theme_customizer( $wp_customize ) {
        $wp_customize->add_setting( 'custom_footer_text',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text',array(
            'title' => __( 'Custom footer text','beogroup' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'custom_footer',array(
            'label' => __( 'Footer Text','beogroup' ),
            'section' => 'footer_text',
            'settings' => 'custom_footer_text',
            'type' => 'textarea'
        ) ) );
        $wp_customize->add_setting( 'custom_footer_text5',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text5',array(
           'title' => __( 'Beo Export Logo','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'custom_footer5',
                array(
                    'label'      => __( 'Upload logo', 'beogroup' ),
                    'section'    => 'footer_text5',
                    'settings'   => 'custom_footer_text5',
                )
            )
        );
        $wp_customize->add_setting( 'custom_footer_text1',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text1',array(
            'title' => __( 'Beo-Travel Logo','beogroup' ),
            'priority' => 160,
        ) );
        $wp_customize->add_control(
           new WP_Customize_Image_Control(
               $wp_customize,
               'custom_footer1',
               array(
                   'label'      => __( 'Upload logo', 'beotravel' ),
                   'section'    => 'footer_text1',
                   'settings'   => 'custom_footer_text1',
               )
           )
        );
        $wp_customize->add_setting( 'custom_footer_text2',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text2',array(
           'title' => __( 'Bendigo Bank Logo','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'custom_footer2',
                array(
                    'label'      => __( 'Upload logo', 'bendigo-bank' ),
                    'section'    => 'footer_text2',
                    'settings'   => 'custom_footer_text2',
                )
            )
        );
        $wp_customize->add_setting( 'custom_footer_text3',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text3',array(
           'title' => __( 'Properties Overseas Logo','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'custom_footer3',
                array(
                    'label'      => __( 'Upload logo', 'bendigo-bank' ),
                    'section'    => 'footer_text3',
                    'settings'   => 'custom_footer_text3',
                )
            )
        );
        $wp_customize->add_setting( 'custom_footer_text4',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text4',array(
           'title' => __( 'Beo Radio Logo','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'custom_footer4',
                array(
                    'label'      => __( 'Upload logo', 'bendigo-bank' ),
                    'section'    => 'footer_text4',
                    'settings'   => 'custom_footer_text4',
                )
            )
        );

        $wp_customize->add_setting( 'custom_footer_text6',array(
           'default' => '',
           'transport' => 'refresh',
        ) );
        $wp_customize->add_section( 'footer_text6',array(
           'title' => __( 'Beo Media Logo','beogroup' ),
           'priority' => 160,
        ) );
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'custom_footer6',
                array(
                    'label'      => __( 'Upload logo', 'bendigo-bank' ),
                    'section'    => 'footer_text6',
                    'settings'   => 'custom_footer_text6',
                )
            )
        );

        $wp_customize->add_section( 'calculator',array(
            'title' => __( 'Calculator','beogroup' ),
            'priority' => 160,
        ) );
        $wp_customize->add_setting( 'calculator_value',array(
            'default' => '',
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control( new WP_Customize_Control( $wp_customize,'value',array(
            'label' => __( 'Value','beogroup' ),
            'section' => 'calculator',
            'settings' => 'calculator_value',
        ) ) );
    }
    add_action( 'customize_register','theme_customizer' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type' ) ) {
    function slider_post_type() {
        $labels = array(
            'name' => __( 'Beogroup Slider', 'beogroup' ),
            'singular_name' => __( 'Beogroup Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider',
            ),
        );
        register_post_type( 'slider', $args );
    }
    add_action( 'init','slider_post_type' );
}

/* Services post type */
if( ! function_exists( 'services_post_type' ) ) {
    function services_post_type() {
        $labels = array(
            'name' => __( 'Beogroup Services', 'beogroup' ),
            'singular_name' => __( 'Beogroup Services', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'services',
            ),
        );
        register_post_type( 'services', $args );
    }
    add_action( 'init','services_post_type' );
}

/* Current offers post type */
if( ! function_exists( 'current_offers_post_type' ) ) {
    function current_offers_post_type() {
        $labels = array(
            'name' => __( 'Beogroup Current offers', 'beogroup' ),
            'singular_name' => __( 'Beogroup Current offers', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'current_offers',
            ),
        );
        register_post_type( 'current_offers', $args );
    }
    add_action( 'init','current_offers_post_type' );
}

/* Gallery post type */
if( ! function_exists( 'gallery_post_type' ) ) {
    function gallery_post_type() {
        $labels = array(
            'name' => __( 'Beogroup Gallery', 'beogroup' ),
            'singular_name' => __( 'Beogroup Gallery', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'gallery',
            ),
        );
        register_post_type( 'gallery', $args );
    }
    add_action( 'init','gallery_post_type' );
}

/* FAQ post type */
if( ! function_exists( 'faq_post_type' ) ) {
    function faq_post_type() {
        $labels = array(
            'name' => __( 'Beogroup FAQ', 'beogroup' ),
            'singular_name' => __( 'Beogroup FAQ', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'faq',
            ),
        );
        register_post_type( 'faq', $args );
    }
    add_action( 'init','faq_post_type' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type1' ) ) {
    function slider_post_type1() {
        $labels = array(
            'name' => __( 'Beotravel Slider', 'beogroup' ),
            'singular_name' => __( 'Beotravel Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider1',
            ),
        );
        register_post_type( 'slider1', $args );
    }
    add_action( 'init','slider_post_type1' );
}

/* Our team post type */
if( ! function_exists( 'ourteam_post_type' ) ) {
    function ourteam_post_type() {
        $labels = array(
            'name' => __( 'Beotravel team', 'beotravel' ),
            'singular_name' => __( 'Beotravel team', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'ourteam',
            ),
        );
        register_post_type( 'ourteam', $args );
    }
    add_action( 'init','ourteam_post_type' );
}

/* References post type */
if( ! function_exists( 'references_post_type' ) ) {
    function references_post_type() {
        $labels = array(
            'name' => __( 'Beotravel References', 'beotravel' ),
            'singular_name' => __( 'Beotravel References', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'references',
            ),
        );
        register_post_type( 'references', $args );
    }
    add_action( 'init','references_post_type' );
}

/* Cruises post type */
if( ! function_exists( 'specials_post_type' ) ) {
    function specials_post_type() {
        $labels = array(
            'name' => __( 'Beotravel Specials', 'beotravel' ),
            'singular_name' => __( 'Beotravel Specials', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'specials',
            ),
        );
        register_post_type( 'specials', $args );
    }
    add_action( 'init','specials_post_type' );
}

/* Cruises post type */
if( ! function_exists( 'cruises_post_type' ) ) {
    function cruises_post_type() {
        $labels = array(
            'name' => __( 'Beotravel Cruises', 'beotravel' ),
            'singular_name' => __( 'Beotravel Cruises', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'cruises',
            ),
        );
        register_post_type( 'cruises', $args );
    }
    add_action( 'init','cruises_post_type' );
}

/* Events post type */
if( ! function_exists( 'events_post_type' ) ) {
    function events_post_type() {
        $labels = array(
            'name' => __( 'Beotravel Events', 'beotravel' ),
            'singular_name' => __( 'Beotravel Events', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'events',
            ),
        );
        register_post_type( 'events', $args );
    }
    add_action( 'init','events_post_type' );
}

/* Rent a car post type */
if( ! function_exists( 'rent_a_car_post_type' ) ) {
    function rent_a_car_post_type() {
        $labels = array(
            'name' => __( 'Beotravel Rent a Car', 'beotravel' ),
            'singular_name' => __( 'Beotravel Rent a Car', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'rent_a_car',
            ),
        );
        register_post_type( 'rent_a_car', $args );
    }
    add_action( 'init','rent_a_car_post_type' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type2' ) ) {
    function slider_post_type2() {
        $labels = array(
            'name' => __( 'Bendigo Bank Slider', 'beogroup' ),
            'singular_name' => __( 'Bendigo Bank Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider2',
            ),
        );
        register_post_type( 'slider2', $args );
    }
    add_action( 'init','slider_post_type2' );
}

/* Services post type */
if( ! function_exists( 'services_post_type1' ) ) {
    function services_post_type1() {
        $labels = array(
            'name' => __( 'Bendigo Bank Services', 'beogroup' ),
            'singular_name' => __( 'Bendigo Bank Services', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'services1',
            ),
        );
        register_post_type( 'services1', $args );
    }
    add_action( 'init','services_post_type1' );
}

/* Services post type */
if( ! function_exists( 'bendigo_bank_services_post_type' ) ) {
    function bendigo_bank_services_post_type() {
        $labels = array(
            'name' => __( 'Bendigo Bank Services 2', 'beogroup' ),
            'singular_name' => __( 'Bendigo Bank Services 2', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'bendigo_bank',
            ),
        );
        register_post_type( 'bendigo_bank', $args );
    }
    add_action( 'init','bendigo_bank_services_post_type' );
}

/* Bendigo Bank our team post type */
if( ! function_exists( 'bendigo_bank_our_team_post_type' ) ) {
    function bendigo_bank_our_team_post_type() {
        $labels = array(
            'name' => __( 'Bendigo Bank Team', 'beogroup' ),
            'singular_name' => __( 'Bendigo Bank Team', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'bendigobank_ourteam',
            ),
        );
        register_post_type( 'bendigobank_ourteam', $args );
    }
    add_action( 'init','bendigo_bank_our_team_post_type' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type3' ) ) {
    function slider_post_type3() {
        $labels = array(
            'name' => __( 'Properties Overseas Slider', 'beogroup' ),
            'singular_name' => __( 'Properties Overseas Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider3',
            ),
        );
        register_post_type( 'slider3', $args );
    }
    add_action( 'init','slider_post_type3' );
}

/* Special offer post type */
if( ! function_exists( 'special_offer_post_type' ) ) {
    function special_offer_post_type() {
        $labels = array(
            'name' => __( 'Real Estate', 'beogroup' ),
            'singular_name' => __( 'Real Estate', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'special_offer',
            ),
        );
        register_post_type( 'special_offer', $args );
    }
    add_action( 'init','special_offer_post_type' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type4' ) ) {
    function slider_post_type4() {
        $labels = array(
            'name' => __( 'Beo Radio Slider', 'beogroup' ),
            'singular_name' => __( 'Beo Radio Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider4',
            ),
        );
        register_post_type( 'slider4', $args );
    }
    add_action( 'init','slider_post_type4' );
}

/* Our team post type */
if( ! function_exists( 'beoradio_our_team_post_type' ) ) {
    function beoradio_our_team_post_type() {
        $labels = array(
            'name' => __( 'Beo Radio Our Team', 'beogroup' ),
            'singular_name' => __( 'Beo Radio Our Team', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'beoradio_our_team',
            ),
        );
        register_post_type( 'beoradio_our_team', $args );
    }
    add_action( 'init','beoradio_our_team_post_type' );
}

/* Register slider */
if( ! function_exists( 'slider_post_type5' ) ) {
    function slider_post_type5() {
        $labels = array(
            'name' => __( 'Beoexport Slider', 'beogroup' ),
            'singular_name' => __( 'Beoexport Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider5',
            ),
        );
        register_post_type( 'slider5', $args );
    }
    add_action( 'init','slider_post_type5' );
}

/* Our team post type */
if( ! function_exists( 'beoexport_ourteam_post_type' ) ) {
    function beoexport_ourteam_post_type() {
        $labels = array(
            'name' => __( 'Beoexport team', 'beotravel' ),
            'singular_name' => __( 'Beoexport team', 'beotravel' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'beoexport_ourteam',
            ),
        );
        register_post_type( 'beoexport_ourteam', $args );
    }
    add_action( 'init','beoexport_ourteam_post_type' );
}

/* Beo Media slider */
if( ! function_exists( 'slider_beomedia_post_type' ) ) {
    function slider_beomedia_post_type() {
        $labels = array(
            'name' => __( 'Beo Media Slider', 'beogroup' ),
            'singular_name' => __( 'Beo Media Slider', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider_beomedia',
            ),
        );
        register_post_type( 'slider_beomedia', $args );
    }
    add_action( 'init','slider_beomedia_post_type' );
}

/* Register bottom slider */
if( ! function_exists( 'slider_bottom_post_type' ) ) {
    function slider_bottom_post_type() {
        $labels = array(
            'name' => __( 'Slider bottom', 'beogroup' ),
            'singular_name' => __( 'Slider bottom', 'beogroup' ) ,
        );
        $supports = array(
            'title',
            'editor',
            'thumbnail',
            'post-formats',
        );
        $args = array(
            'labels' => $labels,
            'supports' => $supports,
            'public' => true,
            'capability_type' => 'post',
            'show_ui' => true,
            'has_archive' => false,
            'rewrite' => array(
                'slug' => 'slider_bottom',
            ),
        );
        register_post_type( 'slider_bottom', $args );
    }
    add_action( 'init','slider_bottom_post_type' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box1' ) ) {
    function contact_meta_box1() {
        add_meta_box(
            'contaact_box1',
            'Button',
            'contact_meta_box_callback1',
            'slider1'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box1' );
}
if( ! function_exists( 'contact_meta_box_callback1' ) ) {
    function contact_meta_box_callback1( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data1', 'contact_meta_box_nonce1' );
        $value1 = get_post_meta( $post->ID, '_contact_value_key1', true );
        $url1 = get_post_meta( $post->ID, '_contact_url_value_key1', true );
        echo '<label for="contact_label1">Label: </label>';
        echo '<input type="text" id="contact_label1" name="contact_label1" value="' . esc_attr( $value1 ) . '" size="25" />';
        echo '<br>';
        echo '<label for="contact_url1">Url: </label>';
        echo '<input type="text" id="contact_url1" name="contact_url1" value="' . esc_attr( $url1 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data1' ) ) {
    function contact_meta_box_save_data1( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce1'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce1'], 'contact_meta_box_save_data1' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label1'] ) ) {
            return;
        }
        $data2 = sanitize_text_field( $_POST['contact_label1'] );
        update_post_meta( $post_id, '_contact_value_key1', $data2 );
        $url2 = $_POST['contact_url1'];
        update_post_meta( $post_id, '_contact_url_value_key1', $url2 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data1' );
}

// /* Contact Meta Box */
// if( ! function_exists( 'contact_meta_box333' ) ) {
//     function contact_meta_box333() {
//         add_meta_box(
//             'contaact_box333',
//             'Enquiry',
//             'contact_meta_box_callback333',
//             'cruises'
//         );
//     }
//     add_action( 'add_meta_boxes', 'contact_meta_box333' );
// }
// if( ! function_exists( 'contact_meta_box_callback333' ) ) {
//     function contact_meta_box_callback333( $post ) {
//         wp_nonce_field( 'contact_meta_box_save_data333', 'contact_meta_box_nonce333' );
//         $value2 = get_post_meta( $post->ID, '_contact_value_key444', true );
//         $url2 = get_post_meta( $post->ID, '_contact_url_value_key444', true );
//         echo '<input type="text" id="contact_label444" name="contact_label444" value="' . esc_attr( $value2 ) . '" size="25" />';
//         echo '<br>';
//         echo '<input type="text" id="contact_url444" name="contact_url444" value="' . esc_attr( $url2 ) . '" size="25" />';
//     }
// }
// if( ! function_exists( 'contact_meta_box_save_data333' ) ) {
//     function contact_meta_box_save_data333( $post_id ) {
//         if( ! isset( $_POST['contact_meta_box_nonce333'] ) ) {
//             return;
//         }
//         if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce333'], 'contact_meta_box_save_data333' ) ) {
//             return;
//         }
//         if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
//             return;
//         }
//         if( ! current_user_can( 'edit_post', $post_id ) ) {
//             return;
//         }
//         $data3 = sanitize_text_field( $_POST['contact_label444'] );
//         update_post_meta( $post_id, '_contact_value_key444', $data3 );
//         $url3 = $_POST['contact_url444'];
//         update_post_meta( $post_id, '_contact_url_value_key444', $url3 );
//     }
//     add_action( 'save_post', 'contact_meta_box_save_data333' );
// }

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box2' ) ) {
    function contact_meta_box2() {
        add_meta_box(
            'contaact_box2',
            'Button',
            'contact_meta_box_callback2',
            'slider2'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box2' );
}
if( ! function_exists( 'contact_meta_box_callback2' ) ) {
    function contact_meta_box_callback2( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data2', 'contact_meta_box_nonce2' );
        $value2 = get_post_meta( $post->ID, '_contact_value_key2', true );
        $url2 = get_post_meta( $post->ID, '_contact_url_value_key2', true );
        echo '<label for="contact_label2">Label: </label>';
        echo '<input type="text" id="contact_label2" name="contact_label2" value="' . esc_attr( $value2 ) . '" size="25" />';
        echo '<br>';
        echo '<label for="contact_url2">Url: </label>';
        echo '<input type="text" id="contact_url2" name="contact_url2" value="' . esc_attr( $url2 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data2' ) ) {
    function contact_meta_box_save_data2( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce2'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce2'], 'contact_meta_box_save_data2' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label2'] ) ) {
            return;
        }
        $data3 = sanitize_text_field( $_POST['contact_label2'] );
        update_post_meta( $post_id, '_contact_value_key2', $data3 );
        $url3 = $_POST['contact_url2'];
        update_post_meta( $post_id, '_contact_url_value_key2', $url3 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data2' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box6' ) ) {
    function contact_meta_box6() {
        add_meta_box(
            'contaact_box6',
            'Button',
            'contact_meta_box_callback6',
            array( 'slider3', 'slider4', 'rent_a_car', 'slider_beomedia' )
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box6' );
}
if( ! function_exists( 'contact_meta_box_callback6' ) ) {
    function contact_meta_box_callback6( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data6', 'contact_meta_box_nonce6' );
        $value6 = get_post_meta( $post->ID, '_contact_value_key6', true );
        $url6 = get_post_meta( $post->ID, '_contact_url_value_key6', true );
        echo '<label for="contact_label6">Label: </label>';
        echo '<input type="text" id="contact_label6" name="contact_label6" value="' . esc_attr( $value6 ) . '" size="25" />';
        echo '<br>';
        echo '<label for="contact_url6">Url: </label>';
        echo '<input type="text" id="contact_url6" name="contact_url6" value="' . esc_attr( $url6 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data6' ) ) {
    function contact_meta_box_save_data6( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce6'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce6'], 'contact_meta_box_save_data6' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label6'] ) ) {
            return;
        }
        $data3 = sanitize_text_field( $_POST['contact_label6'] );
        update_post_meta( $post_id, '_contact_value_key6', $data3 );
        $url3 = $_POST['contact_url6'];
        update_post_meta( $post_id, '_contact_url_value_key6', $url3 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data6' );
}

/* Usluge Meta Box */
if( ! function_exists( 'usluge_add_meta_box' ) ) {
    function usluge_add_meta_box() {
        add_meta_box(
            'usluge_box',
            'Content',
            'usluge_meta_box_callback',
            array( 'slider1', 'special_offer' )
        );
    }
    add_action( 'add_meta_boxes','usluge_add_meta_box' );
}
if( ! function_exists( 'usluge_meta_box_callback' ) ) {
    function usluge_meta_box_callback( $post ) {
        wp_nonce_field( 'usluge_meta_box_save_data','usluge_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_usluge_value_key',true );
        wp_editor( $value, 'usluge_field' );
    }
}
if( ! function_exists( 'usluge_meta_box_save_data' ) ) {
    function usluge_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['usluge_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['usluge_meta_box_nonce'],'usluge_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['usluge_field'] ) ) {
            return;
        }
        if( isset( $_POST['usluge_field'] ) ) {
            update_post_meta( $post_id,'_usluge_value_key',$_POST['usluge_field'] );
        }
    }
    add_action( 'save_post','usluge_meta_box_save_data' );
}

/* Contact Meta Box */
// if( ! function_exists( 'contact_meta_box10' ) ) {
//     function contact_meta_box10() {
//         add_meta_box(
//             'contaact_box10',
//             'Button',
//             'contact_meta_box_callback10',
//             'special_offer'
//         );
//     }
//     add_action( 'add_meta_boxes', 'contact_meta_box10' );
// }
// if( ! function_exists( 'contact_meta_box_callback10' ) ) {
//     function contact_meta_box_callback10( $post ) {
//         wp_nonce_field( 'contact_meta_box_save_data10', 'contact_meta_box_nonce10' );
//         $value10 = get_post_meta( $post->ID, '_contact_value_key10', true );
//         $url10 = get_post_meta( $post->ID, '_contact_url_value_key10', true );
//         echo '<label for="contact_label10">Label: </label>';
//         echo '<input type="text" id="contact_label10" name="contact_label10" value="' . esc_attr( $value10 ) . '" size="25" />';
//         echo '<br>';
//         echo '<label for="contact_url10">Url: </label>';
//         echo '<input type="text" id="contact_url10" name="contact_url10" value="' . esc_attr( $url10 ) . '" size="25" />';
//     }
// }
// if( ! function_exists( 'contact_meta_box_save_data10' ) ) {
//     function contact_meta_box_save_data10( $post_id ) {
//         if( ! isset( $_POST['contact_meta_box_nonce10'] ) ) {
//             return;
//         }
//         if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce10'], 'contact_meta_box_save_data10' ) ) {
//             return;
//         }
//         if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
//             return;
//         }
//         if( ! current_user_can( 'edit_post', $post_id ) ) {
//             return;
//         }
//         if( ! isset( $_POST['contact_label10'] ) ) {
//             return;
//         }
//         $data10 = sanitize_text_field( $_POST['contact_label10'] );
//         update_post_meta( $post_id, '_contact_value_key10', $data10 );
//         $url10 = $_POST['contact_url10'];
//         update_post_meta( $post_id, '_contact_url_value_key10', $url10 );
//     }
//     add_action( 'save_post', 'contact_meta_box_save_data10' );
// }

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box3' ) ) {
    function contact_meta_box3() {
        add_meta_box(
            'contaact_box3',
            'Title',
            'contact_meta_box_callback3',
            'slider1'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box3' );
}
if( ! function_exists( 'contact_meta_box_callback3' ) ) {
    function contact_meta_box_callback3( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data3', 'contact_meta_box_nonce3' );
        $value3 = get_post_meta( $post->ID, '_contact_value_key3', true );
        echo '<label for="contact_label3">Title: </label>';
        echo '<input type="text" id="contact_label3" name="contact_label3" value="' . esc_attr( $value3 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data3' ) ) {
    function contact_meta_box_save_data3( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce3'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce3'], 'contact_meta_box_save_data3' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label3'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label3'] );
        update_post_meta( $post_id, '_contact_value_key3', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data3' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box4' ) ) {
    function contact_meta_box4() {
        add_meta_box(
            'contaact_box4',
            'Title',
            'contact_meta_box_callback4',
            'slider2'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box4' );
}
if( ! function_exists( 'contact_meta_box_callback4' ) ) {
    function contact_meta_box_callback4( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data4', 'contact_meta_box_nonce4' );
        $value4 = get_post_meta( $post->ID, '_contact_value_key4', true );
        echo '<label for="contact_label4">Title: </label>';
        echo '<input type="text" id="contact_label4" name="contact_label4" value="' . esc_attr( $value4 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data4' ) ) {
    function contact_meta_box_save_data4( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce4'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce4'], 'contact_meta_box_save_data4' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label4'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label4'] );
        update_post_meta( $post_id, '_contact_value_key4', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data4' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box5' ) ) {
    function contact_meta_box5() {
        add_meta_box(
            'contaact_box5',
            'Title',
            'contact_meta_box_callback5',
            array( 'slider3', 'slider4', 'slider_beomedia' )
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box5' );
}
if( ! function_exists( 'contact_meta_box_callback5' ) ) {
    function contact_meta_box_callback5( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data5', 'contact_meta_box_nonce5' );
        $value5 = get_post_meta( $post->ID, '_contact_value_key5', true );
        echo '<label for="contact_label5">Title: </label>';
        echo '<input type="text" id="contact_label5" name="contact_label5" value="' . esc_attr( $value5 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data5' ) ) {
    function contact_meta_box_save_data5( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce5'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce5'], 'contact_meta_box_save_data5' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label5'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label5'] );
        update_post_meta( $post_id, '_contact_value_key5', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data5' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box7' ) ) {
    function contact_meta_box7() {
        add_meta_box(
            'contaact_box7',
            'Address',
            'contact_meta_box_callback7',
            'special_offer'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box7' );
}
if( ! function_exists( 'contact_meta_box_callback7' ) ) {
    function contact_meta_box_callback7( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data7', 'contact_meta_box_nonce7' );
        $value7 = get_post_meta( $post->ID, '_contact_value_key7', true );
        echo '<input type="text" id="contact_label7" name="contact_label7" value="' . esc_attr( $value7 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data7' ) ) {
    function contact_meta_box_save_data7( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce7'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce7'], 'contact_meta_box_save_data7' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label7'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label7'] );
        update_post_meta( $post_id, '_contact_value_key7', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data7' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box8' ) ) {
    function contact_meta_box8() {
        add_meta_box(
            'contaact_box8',
            'Price',
            'contact_meta_box_callback8',
            'special_offer'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box8' );
}
if( ! function_exists( 'contact_meta_box_callback8' ) ) {
    function contact_meta_box_callback8( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data8', 'contact_meta_box_nonce8' );
        $value8 = get_post_meta( $post->ID, '_contact_value_key8', true );
        echo '<input type="text" id="contact_label8" name="contact_label8" value="' . esc_attr( $value8 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data8' ) ) {
    function contact_meta_box_save_data8( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce8'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce8'], 'contact_meta_box_save_data8' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label8'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label8'] );
        update_post_meta( $post_id, '_contact_value_key8', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data8' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box11' ) ) {
    function contact_meta_box11() {
        add_meta_box(
            'contaact_box11',
            'Living place',
            'contact_meta_box_callback11',
            'special_offer'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box11' );
}
if( ! function_exists( 'contact_meta_box_callback11' ) ) {
    function contact_meta_box_callback11( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data11', 'contact_meta_box_nonce11' );
        $value11 = get_post_meta( $post->ID, '_contact_value_key11', true );
        echo '<input type="text" id="contact_label11" name="contact_label11" value="' . esc_attr( $value11 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data11' ) ) {
    function contact_meta_box_save_data11( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce11'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce11'], 'contact_meta_box_save_data11' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label11'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label11'] );
        update_post_meta( $post_id, '_contact_value_key11', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data11' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box9' ) ) {
    function contact_meta_box9() {
        add_meta_box(
            'contaact_box9',
            'Land space',
            'contact_meta_box_callback9',
            'special_offer'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box9' );
}
if( ! function_exists( 'contact_meta_box_callback9' ) ) {
    function contact_meta_box_callback9( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data9', 'contact_meta_box_nonce9' );
        $value9 = get_post_meta( $post->ID, '_contact_value_key9', true );
        echo '<input type="text" id="contact_label9" name="contact_label9" value="' . esc_attr( $value9 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data9' ) ) {
    function contact_meta_box_save_data9( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce9'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce9'], 'contact_meta_box_save_data9' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label9'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label9'] );
        update_post_meta( $post_id, '_contact_value_key9', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data9' );
}

/* Slider Meta Box */
if( ! function_exists( 'usluge_add_meta_box' ) ) {
    function usluge_add_meta_box() {
        add_meta_box(
            'usluge_box',
            'Text',
            'usluge_meta_box_callback',
            array( 'slider1', 'rent_a_car' )
        );
    }
    add_action( 'add_meta_boxes','usluge_add_meta_box' );
}
if( ! function_exists( 'usluge_meta_box_callback' ) ) {
    function usluge_meta_box_callback( $post ) {
        wp_nonce_field( 'usluge_meta_box_save_data','usluge_meta_box_nonce' );
        $value = get_post_meta( $post->ID,'_usluge_value_key',true );
        wp_editor( $value, 'usluge_field' );
    }
}
if( ! function_exists( 'usluge_meta_box_save_data' ) ) {
    function usluge_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['usluge_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['usluge_meta_box_nonce'],'usluge_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['usluge_field'] ) ) {
            return;
        }
        if( isset( $_POST['usluge_field'] ) ) {
            update_post_meta( $post_id,'_usluge_value_key',$_POST['usluge_field'] );
        }
    }
    add_action( 'save_post','usluge_meta_box_save_data' );
}

/* Slider Meta Box */
if( ! function_exists( 'usluge_add_meta_box1' ) ) {
    function usluge_add_meta_box1() {
        add_meta_box(
            'usluge_box1',
            'Text',
            'usluge_meta_box_callback1',
            'slider2'
        );
    }
    add_action( 'add_meta_boxes','usluge_add_meta_box1' );
}
if( ! function_exists( 'usluge_meta_box_callback1' ) ) {
    function usluge_meta_box_callback1( $post ) {
        wp_nonce_field( 'usluge_meta_box_save_data1','usluge_meta_box_nonce1' );
        $value = get_post_meta( $post->ID,'_usluge_value_key1',true );
        wp_editor( $value, 'usluge_field1' );
    }
}
if( ! function_exists( 'usluge_meta_box_save_data1' ) ) {
    function usluge_meta_box_save_data1( $post_id ) {
        if( ! isset( $_POST['usluge_meta_box_nonce1'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['usluge_meta_box_nonce1'],'usluge_meta_box_save_data1' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['usluge_field1'] ) ) {
            return;
        }
        if( isset( $_POST['usluge_field1'] ) ) {
            update_post_meta( $post_id,'_usluge_value_key1',$_POST['usluge_field1'] );
        }
    }
    add_action( 'save_post','usluge_meta_box_save_data1' );
}

/* Slider Meta Box */
if( ! function_exists( 'usluge_add_meta_box2' ) ) {
    function usluge_add_meta_box2() {
        add_meta_box(
            'usluge_box2',
            'Text',
            'usluge_meta_box_callback2',
            array( 'slider3', 'slider4', 'slider_beomedia' )
        );
    }
    add_action( 'add_meta_boxes','usluge_add_meta_box2' );
}
if( ! function_exists( 'usluge_meta_box_callback2' ) ) {
    function usluge_meta_box_callback2( $post ) {
        wp_nonce_field( 'usluge_meta_box_save_data2','usluge_meta_box_nonce2' );
        $value = get_post_meta( $post->ID,'_usluge_value_key2',true );
        wp_editor( $value, 'usluge_field2' );
    }
}
if( ! function_exists( 'usluge_meta_box_save_data2' ) ) {
    function usluge_meta_box_save_data2( $post_id ) {
        if( ! isset( $_POST['usluge_meta_box_nonce2'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['usluge_meta_box_nonce2'],'usluge_meta_box_save_data2' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post',$post_id ) ) {
            return;
        }
        if( ! isset( $_POST['usluge_field2'] ) ) {
            return;
        }
        if( isset( $_POST['usluge_field2'] ) ) {
            update_post_meta( $post_id,'_usluge_value_key2',$_POST['usluge_field2'] );
        }
    }
    add_action( 'save_post','usluge_meta_box_save_data2' );
}

/* Link Box */
if( ! function_exists( 'contact_meta_box' ) ) {
    function contact_meta_box() {
        add_meta_box(
            'contact_box',
            'Link',
            'contact_meta_box_callback',
            'services'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box' );
}
if( ! function_exists( 'contact_meta_box_callback' ) ) {
    function contact_meta_box_callback( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data', 'contact_meta_box_nonce' );
        $url = get_post_meta( $post->ID, '_contact_url_value_key', true );
        echo '<label for="contact_url">Url: </label>';
        echo '<input type="text" id="contact_url" name="contact_url" value="' . esc_attr( $url ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data' ) ) {
    function contact_meta_box_save_data( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce'], 'contact_meta_box_save_data' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_url'] ) ) {
            return;
        }
        $url1 = $_POST['contact_url'];
        update_post_meta( $post_id, '_contact_url_value_key', $url1 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box12' ) ) {
    function contact_meta_box12() {
        add_meta_box(
            'contaact_box12',
            'Link',
            'contact_meta_box_callback12',
            'slider'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box12' );
}
if( ! function_exists( 'contact_meta_box_callback12' ) ) {
    function contact_meta_box_callback12( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data12', 'contact_meta_box_nonce12' );
        $value12 = get_post_meta( $post->ID, '_contact_value_key12', true );
        echo '<label for="contact_label12">Link: </label>';
        echo '<input type="text" id="contact_label12" name="contact_label12" value="' . esc_attr( $value12 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data12' ) ) {
    function contact_meta_box_save_data12( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce12'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce12'], 'contact_meta_box_save_data12' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label12'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label12'] );
        update_post_meta( $post_id, '_contact_value_key12', $data4 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data12' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box1234' ) ) {
    function contact_meta_box1234() {
        add_meta_box(
            'contaact_box1234',
            'Info',
            'contact_meta_box_callback1234',
            'cruises'
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box1234' );
}
if( ! function_exists( 'contact_meta_box_callback1234' ) ) {
    function contact_meta_box_callback1234( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data1234', 'contact_meta_box_nonce1234' );
        $value12 = get_post_meta( $post->ID, '_contact_value_key1234', true );
        $duration = get_post_meta( $post->ID, '_duration_value_key', true );
        $call = get_post_meta( $post->ID, '_call_value_key', true );
        $for_sale = get_post_meta( $post->ID, '_for_sale_value_key', true );
        echo '<label for="contact_label1234">Price: </label>';
        echo '<input type="text" id="contact_label1234" name="contact_label1234" value="' . esc_attr( $value12 ) . '" size="25" />';
        echo '<br>';
        echo '<label for="duration_label">Duration: </label>';
        echo '<input type="text" id="duration_label" name="duration_label" value="' . esc_attr( $duration ) . '" size="25" />';
        echo '<br>';
        echo '<label for="call_label">Call us on: </label>';
        echo '<input type="text" id="call_label" name="call_label" value="' . esc_attr( $call ) . '" size="25" />';
        echo '<br>';
        echo '<label for="for_sale_label">Valid for sale: </label>';
        echo '<input type="text" id="for_sale_label" name="for_sale_label" value="' . esc_attr( $for_sale ) . '" size="25" />';
        echo '<br>';
    }
}
if( ! function_exists( 'contact_meta_box_save_data1234' ) ) {
    function contact_meta_box_save_data1234( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce1234'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce1234'], 'contact_meta_box_save_data1234' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label1234'] ) ) {
            return;
        }
        $data4 = sanitize_text_field( $_POST['contact_label1234'] );
        update_post_meta( $post_id, '_contact_value_key1234', $data4 );
        $duration_data = sanitize_text_field( $_POST['duration_label'] );
        update_post_meta( $post_id, '_duration_value_key', $duration_data );
        $call_data = sanitize_text_field( $_POST['call_label'] );
        update_post_meta( $post_id, '_call_value_key', $call_data );
        $for_sale_data = sanitize_text_field( $_POST['for_sale_label'] );
        update_post_meta( $post_id, '_for_sale_value_key', $for_sale_data );
    }
    add_action( 'save_post', 'contact_meta_box_save_data1234' );
}

/* Contact Meta Box */
if( ! function_exists( 'contact_meta_box321' ) ) {
    function contact_meta_box321() {
        add_meta_box(
            'contaact_box321',
            'Upload Image',
            'contact_meta_box_callback321',
            array( 'services' )
        );
    }
    add_action( 'add_meta_boxes', 'contact_meta_box321' );
}
if( ! function_exists( 'contact_meta_box_callback321' ) ) {
    function contact_meta_box_callback321( $post ) {
        wp_nonce_field( 'contact_meta_box_save_data321', 'contact_meta_box_nonce321' );
        $value3 = get_post_meta( $post->ID, '_contact_value_key321', true );
        echo '<label for="contact_label321">Image link: </label>';
        echo '<input type="text" id="contact_label321" name="contact_label321" value="' . esc_attr( $value3 ) . '" size="25" />';
    }
}
if( ! function_exists( 'contact_meta_box_save_data321' ) ) {
    function contact_meta_box_save_data321( $post_id ) {
        if( ! isset( $_POST['contact_meta_box_nonce321'] ) ) {
            return;
        }
        if( ! wp_verify_nonce( $_POST['contact_meta_box_nonce321'], 'contact_meta_box_save_data321' ) ) {
            return;
        }
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if( ! isset( $_POST['contact_label321'] ) ) {
            return;
        }
        $data2 = sanitize_text_field( $_POST['contact_label321'] );
        update_post_meta( $post_id, '_contact_value_key321', $data2 );
    }
    add_action( 'save_post', 'contact_meta_box_save_data321' );
}

function create_category_taxonomy() {
	$labels = array(
		'name'              => _x( 'Category', 'taxonomy general name', 'beogroup' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'beogroup' ),
		'search_items'      => __( 'Search Category', 'beogroup' ),
		'all_items'         => __( 'All Category', 'beogroup' ),
		'parent_item'       => __( 'Parent Category', 'beogroup' ),
		'parent_item_colon' => __( 'Parent Category:', 'beogroup' ),
		'edit_item'         => __( 'Edit Category', 'beogroup' ),
		'update_item'       => __( 'Update Category', 'beogroup' ),
		'add_new_item'      => __( 'Add New Category', 'beogroup' ),
		'new_item_name'     => __( 'New Category Name', 'beogroup' ),
		'menu_name'         => __( 'Category', 'beogroup' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'faq_category' ),
	);
	register_taxonomy( 'faq_category', array( 'faq' ), $args );

}
add_action( 'init', 'create_category_taxonomy' );

function create_property_taxonomy() {
	$labels = array(
		'name'              => _x( 'Property type', 'taxonomy general name', 'beogroup' ),
		'singular_name'     => _x( 'Property type', 'taxonomy singular name', 'beogroup' ),
		'search_items'      => __( 'Search Properties', 'beogroup' ),
		'all_items'         => __( 'All Properties', 'beogroup' ),
		'parent_item'       => __( 'Parent Property', 'beogroup' ),
		'parent_item_colon' => __( 'Parent Property:', 'beogroup' ),
		'edit_item'         => __( 'Edit Property', 'beogroup' ),
		'update_item'       => __( 'Update Property', 'beogroup' ),
		'add_new_item'      => __( 'Add New Property', 'beogroup' ),
		'new_item_name'     => __( 'New Property Name', 'beogroup' ),
		'menu_name'         => __( 'Property type', 'beogroup' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'property' ),
	);
	register_taxonomy( 'property', array( 'special_offer' ), $args );

}
add_action( 'init', 'create_property_taxonomy' );

function create_type_taxonomy() {
	$labels = array(
		'name'              => _x( 'Selling/Renting', 'taxonomy general name', 'beogroup' ),
		'singular_name'     => _x( 'Selling/Renting', 'taxonomy singular name', 'beogroup' ),
		'search_items'      => __( 'Search', 'beogroup' ),
		'all_items'         => __( 'All', 'beogroup' ),
		'parent_item'       => __( 'Parent', 'beogroup' ),
		'parent_item_colon' => __( 'Parent', 'beogroup' ),
		'edit_item'         => __( 'Edit', 'beogroup' ),
		'update_item'       => __( 'Update', 'beogroup' ),
		'add_new_item'      => __( 'Add New', 'beogroup' ),
		'new_item_name'     => __( 'New Name', 'beogroup' ),
		'menu_name'         => __( 'Selling/Renting', 'beogroup' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'type' ),
	);
	register_taxonomy( 'type', array( 'special_offer' ), $args );

}
add_action( 'init', 'create_type_taxonomy' );

// Register widgets
function addWidget(){
    register_sidebar( array (
        'name' => 'Footer image',
        'id' => 'footer1',
        'before_widget' => '<div>',
		'after_widget' => '</div>',
    ));
    register_sidebar( array (
        'name' => 'Navigation',
        'id' => 'footer2',
        'before_widget' => '<div>',
		'after_widget' => '</div>',
    ));
    register_sidebar( array (
        'name' => 'Links',
        'id' => 'footer3',
        'before_widget' => '<div class="footero">',
		'after_widget' => '</div>',
        'before_title'  => '<p class="">',
    	'after_title'   => '</p>'
    ));
    register_sidebar( array (
        'name' => 'Brochure',
        'id' => 'footer4',
        'before_widget' => '<div class="footer-brochure">',
		'after_widget' => '</div>',
    ));
}
add_action( 'widgets_init', 'addWidget' );

if ( ! function_exists( 'vc_before_init_actions' ) ) {
    function vc_before_init_actions() {
        require_once( get_template_directory() . '/vc-templates/slider.php' );
        require_once( get_template_directory() . '/vc-templates/services-min.php' );
        require_once( get_template_directory() . '/vc-templates/services-min2.php' );
        require_once( get_template_directory() . '/vc-templates/homepage-about.php' );
        require_once( get_template_directory() . '/vc-templates/slider2.php' );
        require_once( get_template_directory() . '/vc-templates/services3.php' );
        require_once( get_template_directory() . '/vc-templates/aboutus.php' );
        require_once( get_template_directory() . '/vc-templates/currentoffers.php' );
        require_once( get_template_directory() . '/vc-templates/gallery.php' );
        require_once( get_template_directory() . '/vc-templates/faq.php' );
        require_once( get_template_directory() . '/vc-templates/map.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-slider.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-services.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-text.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-ourteam.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-textimage.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-references.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-slider.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-text.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-textimage.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-services.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-slider.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-overseas-representative.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-textimage.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-special-offer.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-search.php' );
        require_once( get_template_directory() . '/vc-templates/properties-overseas-offer.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-slider.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-services.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-text.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-textimage.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-broadcast.php' );
        require_once( get_template_directory() . '/vc-templates/beoradio-ourteam.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-faq.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-specials.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-cruises.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-events.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-airfares.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-textbutton.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-rent-a-car.php' );
        require_once( get_template_directory() . '/vc-templates/beotravel-textimagebutton.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-liverpoolbank.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-services2.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-reward.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-ourteam.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-currentoffer.php' );
        require_once( get_template_directory() . '/vc-templates/bendigobank-services3.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-slider.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-nav.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-text.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-services.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-textimage.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-text2.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-text3.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-text4.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-title.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-title2.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-imagetext.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-buttons.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-textimage2.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-ourteam.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-faq.php' );
        require_once( get_template_directory() . '/vc-templates/beomedia-slider.php' );
        require_once( get_template_directory() . '/vc-templates/beomedia-home.php' );
        require_once( get_template_directory() . '/vc-templates/beomedia-services.php' );
        require_once( get_template_directory() . '/vc-templates/beoexport-calculator.php' );
    }
    add_action( 'vc_before_init', 'vc_before_init_actions' );
}

function setPostViews($postID) {
    $countKey = 'post_views_count';
    $count = get_post_meta($postID, $countKey, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $countKey);
        add_post_meta($postID, $countKey, '0');
    }else{
        $count++;
        update_post_meta($postID, $countKey, $count);
    }
}

add_shortcode( 'bartag', 'bartag_func' );
function bartag_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'image' => '',
   ), $atts ) );

   $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
   ?>
   <div id="title" class="about about-director" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'full', false )[0]; ?>);">
       <div class="container home-about-container clearfix">
           <div class="home-about-text col-xs-12 col-sm-12 col-md-12 col-lg-8">
               <p><?php echo $content; ?></p>
           </div>
       </div>
   </div>
   <?php
}

add_action( 'vc_before_init', 'your_name_integrateWithVC' );
function your_name_integrateWithVC() {
   vc_map( array(
      "name" => __( "About us", "my-text-domain" ),
      "base" => "bartag",
      "class" => "",
      "category" => __( "Beogroup elements", "my-text-domain"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
          array(
              'type' => 'attach_image',
              'holder' => 'figure',
              'class' => 'image',
              'heading' => __( 'Image', 'beogroup' ),
              'param_name' => 'image',
              'value' => '',
              'description' => '',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Background image',
          ),
          array(
              "type" => "textarea_html",
              "holder" => "div",
              "class" => "",
              "heading" => __( "Content", "my-text-domain" ),
              "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
              "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
              "description" => __( "Enter your content.", "my-text-domain" ),
              'group' => 'Content',
          )
      )
   ) );
}

add_shortcode( 'bartag2', 'bartag_func2' );
function bartag_func2( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
   ), $atts ) );

   $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
   ?>
   <div class="container to-bosna-container">
       <p><?php echo $content; ?></p>
   </div>
   <?php
}

add_action( 'wp_ajax_realestate', 'realestates_form' );
add_action( 'wp_ajax_nopriv_realestate', 'realestate_form' );
function realestate_form() {

   require_once __DIR__ . '/inc/contactform2.php';
   exit('okej');
}

add_action( 'vc_before_init', 'your_name_integrateWithVC2' );
function your_name_integrateWithVC2() {
   vc_map( array(
      "name" => __( "Content", "my-text-domain" ),
      "base" => "bartag2",
      "class" => "",
      "category" => __( "Beo Export elements", "my-text-domain"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
          array(
              "type" => "textarea_html",
              "holder" => "div",
              "class" => "",
              "heading" => __( "Content", "my-text-domain" ),
              "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
              "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
              "description" => __( "Enter your content.", "my-text-domain" ),
              'group' => 'Test',
          )
      )
   ) );
}

add_shortcode( 'bartag123', 'bartag_func123' );
function bartag_func123( $atts, $content = null ) { // New function parameter $content is added!
    extract( shortcode_atts( array(
       'image' => '',
    ), $atts ) );

   $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
   ?>
   <div class="beotravel-plane" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'bgimage2', false )[0]; ?>);">
       <div class="container">
           <p><?php echo $content; ?></p>
       </div>
   </div>
   <?php
}

add_action( 'vc_before_init', 'your_name_integrateWithVC123' );
function your_name_integrateWithVC123() {
   vc_map( array(
      "name" => __( "Content 2", "my-text-domain" ),
      "base" => "bartag123",
      "class" => "",
      "category" => __( "Beo Export elements", "my-text-domain"),
      'admin_enqueue_js' => array(get_template_directory_uri().'/vc_extend/bartag.js'),
      'admin_enqueue_css' => array(get_template_directory_uri().'/vc_extend/bartag.css'),
      "params" => array(
          array(
              'type' => 'attach_image',
              'holder' => 'figure',
              'class' => 'image',
              'heading' => __( 'Image', 'beogroup' ),
              'param_name' => 'image',
              'value' => '',
              'description' => '',
              'admin_label' => false,
              'weight' => 0,
              'group' => 'Background image',
          ),
          array(
              "type" => "textarea_html",
              "holder" => "div",
              "class" => "",
              "heading" => __( "Content", "my-text-domain" ),
              "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
              "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
              "description" => __( "Enter your content.", "my-text-domain" ),
              'group' => 'Text',
          )
      )
   ) );
}

function wpcodex_add_excerpt_support_for_post() {
    add_post_type_support( 'cruises', 'excerpt' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_post' );

include __DIR__ . '/inc/social-network.php';

add_filter( 'pre_option_link_manager_enabled', '__return_true' );
