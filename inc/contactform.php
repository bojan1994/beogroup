<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ . '/../vendor/autoload.php';

$secret = '6Ld0GW0UAAAAAJeYRXPyiaJqbOwvAWp2-6NJhSML';
$gRecaptchaResponse = $_POST['g-recaptcha-response'];
$remoteIp = $_SERVER['REMOTE_ADDR'];

if(isset($_POST['btn_submit'])) {
    if(!empty($_POST['fullname']) && !empty($_POST['email']) && !empty($_POST['subject']) && !empty($_POST['message'])) {
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $recaptcha = new \ReCaptcha\ReCaptcha($secret);
            $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
            if ($resp->isSuccess()) {
                $fullname = strip_tags($_POST['fullname']);
                $email = strip_tags($_POST['email']);
                $subject = strip_tags($_POST['subject']);
                $message = strip_tags($_POST['message']);
                $mail = new PHPMailer(true);
                $mail->setFrom($email, $fullname, 0);
                $mail->addAddress('bojan.mihajlovic@m1.rs', 'BEOGroup');
                $mail->addReplyTo($email, $fullname);
                $mail->isHTML(true);
                $mail->Subject = $subject;
                $mail->Body    = 'Fullname: ' . $fullname . '<br>';
                $mail->Body    .= 'E-mail: ' . $email . '<br>';
                $mail->Body    .= 'Message: ' . $message;
                $mail->AltBody = $message;
                $success = $mail->send();
                if($success) {
                    echo '<script>alert("Your message has been sent.")</script>';
                } else {
                    echo '<script>alert("There was an error sending this message.")</script>';
                }
            } else {
                echo '<script>alert("reCAPTCHA is required.")</script>';
            }
        } else {
            echo '<script>alert("Invalid e-mail address.")</script>';
        }
    } else {
        echo '<script>alert("All fields are required.")</script>';
    }
}
