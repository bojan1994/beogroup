<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__ . '/../vendor/autoload.php';
$id = $_POST['id'];
if($id == "realestate") {
    if(!empty($_POST['name_']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['message'])) {
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            if(is_numeric($_POST['phone'])) {
                $name = strip_tags($_POST['name_']);
                $email = strip_tags($_POST['email']);
                $phone = strip_tags($_POST['phone']);
                $message = strip_tags($_POST['message']);
                $property = strip_tags($_POST['property']);
                $mail = new PHPMailer(true);
                $mail->setFrom($email, $name, 0);
                $mail->addAddress('bojan.mihajlovic@m1.rs', 'BEOGroup');
                $mail->addReplyTo($email, $name);
                $mail->isHTML(true);
                $mail->Subject = 'Real Estate Property';
                $mail->Body   = 'Name: ' . $name;
                $mail->Body   .= '<br>';
                $mail->Body   .= 'E-mail: ' . $email;
                $mail->Body   .= '<br>';
                $mail->Body   .= 'Phone: ' . $phone;
                $mail->Body   .= '<br>';
                $mail->Body   .= 'Message: ' . $message;
                $mail->Body   .= '<br>';
                $mail->Body   .= 'Property: ' . $property;
                $mail->Body   .= '<br>';
                $mail->AltBody = $message;
                $success = $mail->send();
                if($success) {
                    echo '<script>alert("Your message has been sent.")</script>';
                } else {
                    echo '<script>alert("There was an error sending this message.")</script>';
                }
            }
        }
    }
}
