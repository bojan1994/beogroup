module.exports = function(grunt) {

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "style.css": "assets/less/main.less" // destination file and source file
        }
      }
    },
    copy: {
        js: {
            src: 'node_modules/slick-carousel/slick/slick.min.js',
            dest: 'assets/js/slick.min.js',
            src: 'node_modules/lightbox2/dist/js/lightbox.min.js',
            dest: 'assets/js/lightbox.min.js',
        },
        css: {
            src: 'node_modules/slick-carousel/slick/slick.css',
            dest: 'assets/css/slick.css',
            src: 'node_modules/lightbox2/dist/css/lightbox.min.css',
            dest: 'assets/css/lightbox.min.css',
        },
    },
    watch: {
      styles: {
        files: ['assets/less/**/*.less'], // which files to watch
        tasks: ['less', 'copy'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.registerTask('default', ['watch']);
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
};
