        <footer class="site-footer">
            <div class="container footer-container">
                <div class="footer-logo">
                    <?php
                    if( is_active_sidebar( 'footer1' )) {
                        dynamic_sidebar( 'footer1' );
                    }
                    ?>
                </div>
                <div class="footer-nav">
                    <?php
                    if( is_active_sidebar( 'footer2' )) {
                        dynamic_sidebar( 'footer2' );
                    }
                    ?>
                </div>
                <div class="footer-links clearfix">
                    <?php
                    if( is_active_sidebar( 'footer3' )) {
                        dynamic_sidebar( 'footer3' );
                    }
                    ?>
                </div>
                <div class="footer-search clearfix">
                    <div class="tnp tnp-subscription">
                        <h4>Newsletter</h4>
                        <form method="post" action="https://dev1.m1.rs/beogroup/" onsubmit="return newsletter_check(this)">
                            <input type="hidden" name="nlang" value="">
                            <div class="tnp-field tnp-field-email"><input class="tnp-email" type="email" name="ne" placeholder="Your E-mail" required></div>
                            <div class="tnp-field tnp-field-button"><input class="tnp-submit" type="submit" value="Subscribe"></div>
                        </form>
                    </div>
                    <?php
                    if( is_active_sidebar( 'footer4' )) {
                        dynamic_sidebar( 'footer4' );
                    }
                    ?>
                    <div class="footer-find">
                        <p>Find us on</p>
                        <?php
                        $args = array(
                            'theme_location' => 'social',
                            'walker' => new Social_Network_Nav_Menu(),
                        );
                        wp_nav_menu( $args );
                        ?>
                    </div>
                </div>

            </div>
            <div class="footer-bottom">
                <div class="footer-bottom-container container">
                    <p class="footer-paragraph">This information has been prepared for distribution over the internet and without taking into account financial situation and particular needs of any particular person. BEO-Export makes no recommendations as to the merits of any financial product referred to in this website, emails or its related websites. BEO-EXPORT Australia Pty Ltd - ABN: 55 074 232 830. BEO-EXPORT Australia Pty Ltd has an Australian Financial Services Licence (AFSL number 294171) to deal in foreign exchange issued and regulated in Australia by Australian Securities and Investment Commission (ASIC). For more info please download our <a style="text-decoration: underline !important" href="http://dev1.m1.rs/beogroup/wp-content/uploads/2018/12/BeoExport_FSGPDS_Nov2018_PRINT.pdf" target="_blank">Combined FSG & PDS</a> <a href="http://dev1.m1.rs/beogroup/wp-content/uploads/2018/09/BEO-Export_Terms_and_Condtions_Sep_2014_Version_2_2.pdf" style="text-decoration: underline !important" target="_blank">Terms & Conditions</a> <a style="text-decoration: underline !important" href="http://dev1.m1.rs/beogroup/wp-content/uploads/2018/09/BEOE001_11232_020_Privacy-Policy_2018-07-11.pdf" target="_blank">Privacy Policy</a> <br><br>
                    &copy; Copyright 2018. All Rights Reserved. Webiste by <a style="text-decoration: underline !important" href="https://www.m1.rs" target="_blank">New Look Entertainment</a>.</p>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>

</html>
