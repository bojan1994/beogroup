<?php

class vcBeoexportTextImage2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_textimage2_mapping' ) );
        add_shortcode( 'vc_beoexport_textimage2', array( $this, 'vc_beoexport_textimage2_html' ) );
    }
    public function vc_beoexport_textimage2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Statistics', 'beotravel' ),
                'base' => 'vc_beoexport_textimage2',
                'description' => __( 'Statistics', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 1',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 2',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 3',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Section 3',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Background image',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_textimage2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'title2' => '',
                    'text2' => '',
                    'title3' => '',
                    'text3' => '',
                    'image' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beoexport-company-blue" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'bgimage2', false )[0]; ?>)">
            <div class="container beoexport-company-blue-container">
                <div class="col-xs-12 col-sm-4 col-md-4 beoexport-company-blue-item">
                    <span><?php echo $title; ?></span>
                    <p><?php echo $text; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 beoexport-company-blue-item">
                    <span><?php echo $title2; ?></span>
                    <p><?php echo $text2; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 beoexport-company-blue-item">
                    <span><?php echo $title3; ?></span>
                    <p><?php echo $text3; ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportTextImage2();
