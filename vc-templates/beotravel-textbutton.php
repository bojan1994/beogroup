<?php

class vcBeotravelTextButton extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_textbutton_mapping' ) );
        add_shortcode( 'vc_beotravel_textbutton', array( $this, 'vc_beotravel_textbutton_html' ) );
    }
    public function vc_beotravel_textbutton_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text with button', 'beotravel' ),
                'base' => 'vc_beotravel_textbutton',
                'description' => __( 'Text with button', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_textbutton_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'label' => '',
                    'url' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="title" class="white-section">
            <div class="container white-section-container">
                <div class="white-section-left col-xs-12 col-sm-6 col-md-8">
                    <h2><?php echo $title; ?></h2>
                    <p><?php echo $text; ?></p>
                </div>
                <div class="white-section-button col-xs-12 col-sm-6 col-md-4">
                    <a class="blue-button white-section-link" href="<?php echo $url; ?>"><?php echo $label; ?></a>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeotravelTextButton();
