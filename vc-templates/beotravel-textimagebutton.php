<?php

class vcBeotravelTextImageButton extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_textimagebutton_mapping' ) );
        add_shortcode( 'vc_beotravel_textimagebutton', array( $this, 'vc_beotravel_textimagebutton_html' ) );
    }
    public function vc_beotravel_textimagebutton_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text 2', 'beotravel' ),
                'base' => 'vc_beotravel_textimagebutton',
                'description' => __( 'Text 2', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_textimagebutton_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'text' => '',
                    'label' => '',
                    'url' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="check">
            <div class="container check-container">
                <div class="row check-row">
                    <div class="check-column col-md-6 col-xs-12 col-sm-6">
                        <img src="<?php echo wp_get_attachment_image_src( $image, 'full' )[0]; ?>">
                    </div>
                    <div class="check-column col-md-6 col-xs-12 col-sm-6">
                        <p><?php echo $text; ?></p>
                    </div>
                </div>
                <div class="check-button">
                    <a class="blue-button" href="<?php echo $url; ?>"><?php echo $label; ?></a>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeotravelTextImageButton();
