<?php

class vcBendigoBankServices2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_services2_mapping' ) );
        add_shortcode( 'vc_bendigobank_services2', array( $this, 'vc_bendigobank_services2_html' ) );
    }
    public function vc_bendigobank_services2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services', 'beogroup' ),
                'base' => 'vc_bendigobank_services2',
                'description' => __( 'Services', 'beogroup' ),
                'category' => __( 'Bendigo Bank elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beotravel' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Services',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_services2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'bgcolor' => '',
                ),
                $atts
            )
        );
        global $post;
        $args = array(
        	'post_type' => 'bendigo_bank',
            'posts_per_page' => 10,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="bendigo-red">
                <div class="bendigo-red-container container">
                    <div class="row">
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="bendigo-red-single col-xs-12 col-sm-6 col-md-3">
                                <h3><?php the_title(); ?></h3>
                                <?php the_content(); ?>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBendigoBankServices2();
