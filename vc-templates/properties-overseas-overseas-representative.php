<?php

class vcPropertiesOverseasOverseasRepresentative extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_propertiesoverseas_overseasrepresentative_mapping' ) );
        add_shortcode( 'vc_propertiesoverseas_overseasrepresentative', array( $this, 'vc_propertiesoverseas_overseasrepresentative_html' ) );
    }
    public function vc_propertiesoverseas_overseasrepresentative_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Overseas Representative', 'beotravel' ),
                'base' => 'vc_propertiesoverseas_overseasrepresentative',
                'description' => __( 'Overseas Representative', 'beotravel' ),
                'category' => __( 'Properties Overseas elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Title',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Overseas Representative',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Overseas Representative',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'representative',
                        'heading' => __( 'Representative', 'beotravel' ),
                        'param_name' => 'representative',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Overseas Representative',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fullname',
                        'heading' => __( 'Fullname', 'beotravel' ),
                        'param_name' => 'fullname',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Overseas Representative',
                    ),
                )
            )
        );
    }
    public function vc_propertiesoverseas_overseasrepresentative_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'image' => '',
                    'text' => '',
                    'representative' => '',
                    'fullname' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="home" class="beotravel-text overseas-text">
            <div class="beotravel-text-container container overseas-text-container">
                <h2><?php echo $title; ?></h2>
                <div class="home-about-text overseas-text-inner col-12 col-sm-7 col-md-7">
                    <p><?php echo $text ?></p>
                    <div class="profile">
                        <p><?php echo $representative; ?></p>
                        <h4><?php echo $fullname; ?></h4>
                    </div>
                </div>
                <div class="home-about-img overseas-img col-12 col-sm-5 col-md-5">
                    <img src="<?php echo wp_get_attachment_image_src( $image, 'overseas-representative' )[0]; ?>" alt="<?php echo $fullname; ?>" title="<?php echo $fullname; ?>">
                </div>
            </div>
        </div>
        <?php
    }
}

new vcPropertiesOverseasOverseasRepresentative();
