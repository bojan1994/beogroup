<?php

class vcBeoexportText extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_text_mapping' ) );
        add_shortcode( 'vc_beoexport_text', array( $this, 'vc_beoexport_text_html' ) );
    }
    public function vc_beoexport_text_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text', 'beotravel' ),
                'base' => 'vc_beoexport_text',
                'description' => __( 'Text', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_text_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="title" class="beotravel-text beotravel-holiday-text beotravel-cruise-text">
            <div class="beotravel-holiday-container beotravel-cruise-container container">
                <h2><?php echo $title; ?></h2>
                <?php
                if( $text ) {
                    ?>
                    <p><?php echo $text; ?></p>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportText();
