<?php

class vcBeoexportTextImage111 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_textimage111_mapping' ) );
        add_shortcode( 'vc_beoexport_textimage111', array( $this, 'vc_beoexport_textimage111_html' ) );
    }
    public function vc_beoexport_textimage111_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text with image', 'beotravel' ),
                'base' => 'vc_beoexport_textimage111',
                'description' => __( 'Text with image', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Background image',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 2',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_textimage111_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'image' => '',
                    'label' => '',
                    'url' => '',
                    'label2' => '',
                    'url2' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="send-money-blue" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'bgimage2', false )[0]; ?>)">
            <div class="container send-money-blue-container">
                <h4><?php echo $title; ?></h4>
                <div class="send-money-blue-container-paragraph">
                    <p><?php echo $text; ?></p>
                </div>
                <div class="send-money-blue-container-buttons">
                    <div class="col-xs-12 col-md-6 left-baton baton">
                        <a href="<?php echo $url; ?>" target="_blank"><?php echo $label; ?></a>
                    </div>
                    <div class="col-xs-12 col-md-6 right-baton baton">
                        <a href="<?php echo $url2; ?>" target="_blank"><?php echo $label2; ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportTextImage111();
