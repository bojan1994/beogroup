<?php

class vcBeogroupAboutUs extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_aboutus_mapping' ) );
        add_shortcode( 'vc_beogroup_aboutus', array( $this, 'vc_beogroup_aboutus_html' ) );
    }
    public function vc_beogroup_aboutus_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'About us text', 'beogroup' ),
                'base' => 'vc_beogroup_aboutus',
                'description' => __( 'About us text', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Signature',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'signature',
                        'heading' => __( 'Signature', 'beogroup' ),
                        'param_name' => 'signature',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Signature',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_aboutus_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                    'image' => '',
                    'signature' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="sites-container container">
            <p><?php echo $text; ?></p>
            <div class="signature">
                <img src="<?php echo wp_get_attachment_image_src( $image, 'signature' )[0]; ?>" alt="<?php echo $signature; ?>" title="<?php echo $signature; ?>">
                <p><?php echo $signature; ?></p>
            </div>
        </div>
        <?php
    }
}

new vcBeogroupAboutUs();
