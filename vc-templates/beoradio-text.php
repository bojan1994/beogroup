<?php

class vcBeoRadioText extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoradio_text_mapping' ) );
        add_shortcode( 'vc_beoradio_text', array( $this, 'vc_beoradio_text_html' ) );
    }
    public function vc_beoradio_text_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text', 'beotravel' ),
                'base' => 'vc_beoradio_text',
                'description' => __( 'Text', 'beotravel' ),
                'category' => __( 'Beo Radio elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoradio_text_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="radio" class="beotravel-text">
            <div class="beotravel-text-container container">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $text; ?></p>
            </div>
        </div>
        <?php
    }
}

new vcBeoRadioText();
