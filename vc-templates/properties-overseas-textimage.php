<?php

class vcPropertiesOverseasTextImage extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_propertiesoverseas_textimage_mapping' ) );
        add_shortcode( 'vc_propertiesoverseas_textimage', array( $this, 'vc_propertiesoverseas_textimage_html' ) );
    }
    public function vc_propertiesoverseas_textimage_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text with background image', 'beotravel' ),
                'base' => 'vc_propertiesoverseas_textimage',
                'description' => __( 'Text with background image', 'beotravel' ),
                'category' => __( 'Properties Overseas elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Background image',
                    ),
                )
            )
        );
    }
    public function vc_propertiesoverseas_textimage_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                    'image' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beotravel-plane overseas-blue" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'bgimage2', false )[0]; ?>)">
            <div class="container overseas-blue-container">
                <p><?php echo $text; ?></p>
            </div>
        </div>
        <?php
    }
}

new vcPropertiesOverseasTextImage();
