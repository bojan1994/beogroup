<?php

class vcBendigobankTextImage extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_textimage_mapping' ) );
        add_shortcode( 'vc_bendigobank_textimage', array( $this, 'vc_bendigobank_textimage_html' ) );
    }
    public function vc_bendigobank_textimage_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text with background image', 'beogroup' ),
                'base' => 'vc_bendigobank_textimage',
                'description' => __( 'Text with background image', 'beogroup' ),
                'category' => __( 'Bendigo Bank elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Background image',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_textimage_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                    'image' => '',
                ),
                $atts
            )
        );
        ?>


        <div class="home-about bendigo-background-image clearfix">
            <div class="holder">
                <div>
                    &nbsp;
                </div>
                <div style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'full', false )[0]; ?>)">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/triangle.png" alt="triangle">
                </div>
            </div>
            <div class="absolute container">
                <div class="paragraph-text">
                    <p><?php echo $text; ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBendigobankTextImage();
