<?php

class vcBeotravelSpecials extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_specials_mapping' ) );
        add_shortcode( 'vc_beotravel_specials', array( $this, 'vc_beotravel_specials_html' ) );
    }
    public function vc_beotravel_specials_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Specials', 'beotravel' ),
                'base' => 'vc_beotravel_specials',
                'description' => __( 'Specials', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'specials',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Specials',
                    ),
                    // array(
                    //     'type' => 'textfield',
                    //     'holder' => 'h2',
                    //     'class' => 'title',
                    //     'heading' => __( 'Title', 'beogroup' ),
                    //     'param_name' => 'title',
                    //     'value' => '',
                    //     'description' => '',
                    //     'admin_label' => false,
                    //     'weight' => 0,
                    //     'group' => 'Text',
                    // ),
                    // array(
                    //     'type' => 'textarea',
                    //     'holder' => 'p',
                    //     'class' => 'text',
                    //     'heading' => __( 'Text', 'beotravel' ),
                    //     'param_name' => 'text',
                    //     'value' => '',
                    //     'description' => '',
                    //     'admin_label' => false,
                    //     'weight' => 0,
                    //     'group' => 'Text',
                    // ),
                    // array(
                    //     'type' => 'attach_image',
                    //     'holder' => 'figure',
                    //     'class' => 'image',
                    //     'heading' => __( 'Image', 'beotravel' ),
                    //     'param_name' => 'bgimage',
                    //     'value' => '',
                    //     'description' => '',
                    //     'admin_label' => false,
                    //     'weight' => 0,
                    //     'group' => 'Background image',
                    // ),
                )
            )
        );
    }
    public function vc_beotravel_specials_html( $atts ) {
        /*
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                    'title' => '',
                    'bgimage' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beotravel-plane clearfix" style="background-color:#30389e;" >
            <div class="beotravel-plane-text col-xs-12 col-sm-12 col-md-6">
                <div class="beotravel-plane-text-inner">
                    <h3><?php echo $title; ?></h3>
                    <p><?php echo $text; ?></p>
                </div>
            </div>
            <div class="beotravel-plane-img col-xs-12 col-sm-12 col-md-6">

            </div>
        </div>
        <?php
        */
        $args = array(
            'post_type' => 'specials',
        );
        $counter = 0;
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            $id = hash( 'md5', rand( 10, 99 ) . time() );
            ?>
            <div class="beotravel-specials">
                <div id="<?php echo $id; ?>" class="carousel slide specials-slider" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            $backgroundImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'bgimage2' );
                            $counter++;
                            ?>
                            <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?> clearfix">
                                <div class="carousel-caption beotravel-specials-caption">
                                    <div class="beotravel-specials-caption-inner">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <div class="beotravel-specials-img" style="background-image: url('<?php echo $backgroundImage[0]; ?>')">
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                    <a class="left carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeotravelSpecials();
