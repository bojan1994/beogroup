<?php

class vcBeoRadioBroadCast extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoradio_broadcast_mapping' ) );
        add_shortcode( 'vc_beoradio_broadcast', array( $this, 'vc_beoradio_broadcast_html' ) );
    }
    public function vc_beoradio_broadcast_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Broadcasts', 'beotravel' ),
                'base' => 'vc_beoradio_broadcast',
                'description' => __( 'Broadcasts', 'beotravel' ),
                'category' => __( 'Beo Radio elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'date',
                        'heading' => __( 'Date', 'beotravel' ),
                        'param_name' => 'date1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'date',
                        'heading' => __( 'Date', 'beotravel' ),
                        'param_name' => 'date2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'date',
                        'heading' => __( 'Date', 'beotravel' ),
                        'param_name' => 'date3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'date',
                        'heading' => __( 'Date', 'beotravel' ),
                        'param_name' => 'date4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoradio_broadcast_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'date1' => '',
                    'url1' => '',
                    'date2' => '',
                    'url2' => '',
                    'date3' => '',
                    'url3' => '',
                    'date4' => '',
                    'url4' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="demand" class="beotravel-text radio-broadcast">
            <div class="beotravel-text-container container">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $text; ?></p>
                <div class="broadcast">
                    <div class="">
                        <p><?php echo $date1; ?></p>
                        <audio src="<?php echo $url1; ?>" autostart="0" controls="true" volume="1.0"></audio>
                    </div>
                    <div class="">
                        <p><?php echo $date2; ?></p>
                        <audio src="<?php echo $url2; ?>" autostart="0" controls="true" volume="1.0"></audio>
                    </div>
                    <div class="">
                        <p><?php echo $date3; ?></p>
                        <audio src="<?php echo $url3; ?>" autostart="0" controls="true" volume="1.0"></audio>
                    </div>
                    <div class="">
                        <p><?php echo $date4; ?></p>
                        <audio src="<?php echo $url4; ?>" autostart="0" controls="true" volume="1.0"></audio>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoRadioBroadCast();
