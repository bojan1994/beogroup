<?php

class vcBendigobankCurrentOffer extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_currentoffer_mapping' ) );
        add_shortcode( 'vc_bendigobank_currentoffer', array( $this, 'vc_bendigobank_currentoffer_html' ) );
    }
    public function vc_bendigobank_currentoffer_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Current offer', 'beogroup' ),
                'base' => 'vc_bendigobank_currentoffer',
                'description' => __( 'Current offer', 'beogroup' ),
                'category' => __( 'Bendigo Bank elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Offer',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_currentoffer_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                    'image' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="current-offer" class="current-offer">
            <div class="container current-offer-container">
                <div class="beotravel-text">
                    <h2><?php echo $text; ?></h2>
                </div>
                <img src="<?php echo wp_get_attachment_image_src( $image, 'real-estate-current-offer' )[0] ?>">
            </div>
        </div>
        <?php
    }
}

new vcBendigobankCurrentOffer();
