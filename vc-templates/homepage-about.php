<?php

class vcBeogroupHomepageAbout extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_homapageabout_mapping' ) );
        add_shortcode( 'vc_beogroup_homepageabout', array( $this, 'vc_beogroup_homapageabout_html' ) );
    }
    public function vc_beogroup_homapageabout_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Homepage about', 'beogroup' ),
                'base' => 'vc_beogroup_homepageabout',
                'description' => __( 'Homepage about', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beogroup' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beogroup' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image1',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Background image',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beogroup' ),
                        'param_name' => 'label1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beogroup' ),
                        'param_name' => 'url1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Page body',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_homapageabout_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'subtitle' => '',
                    'text' => '',
                    'label' => '',
                    'url' => '',
                    'image1' => '',
                    'image' => '',
                    'label1' => '',
                    'url1' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="about" class="home-about" style="background-image: url(<?php echo wp_get_attachment_image_src( $image, 'bgimage', false )[0]; ?>)">
            <div class="container home-about-container clearfix">
                <div class="home-about-text col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <h2><?php echo $title; ?></h2>
                    <h4><?php echo $subtitle; ?></h4>
                    <p><?php echo $text; ?></p>
                    <?php
                    if( $label && $url ) {
                        ?>
                        <a href="<?php echo $url; ?>" download><?php echo $label; ?></a>
                        <?php
                    }
                    if( $label1 && $url1 ) {
                        ?>
                        <a href="<?php echo $url1; ?>" download><?php echo $label1; ?></a>
                        <?php
                    }
                    ?>
                </div>
                <div class="home-about-img col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <?php

                    if( $image1 ) {
                        ?>
                        <img src="<?php echo wp_get_attachment_image_src( $image1, 'home-image' )[0]; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeogroupHomepageAbout();
