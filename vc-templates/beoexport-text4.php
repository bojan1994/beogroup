<?php

class vcBeoexportText4 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_text4_mapping' ) );
        add_shortcode( 'vc_beoexport_text4', array( $this, 'vc_beoexport_text4_html' ) );
    }
    public function vc_beoexport_text4_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text 4', 'beotravel' ),
                'base' => 'vc_beoexport_text4',
                'description' => __( 'Text 4', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_text4_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beoexport-company-bottom">
            <div class="beoexport-company-bottom-container container">
                <p><?php echo $text; ?></p>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportText4();
