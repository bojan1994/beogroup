<?php

class vcBendigoBankServices extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_services_mapping' ) );
        add_shortcode( 'vc_bendigobank_services', array( $this, 'vc_bendigobank_services_html' ) );
    }
    public function vc_bendigobank_services_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Bendigo Bank services', 'beotravel' ),
                'base' => 'vc_bendigobank_services',
                'description' => __( 'Bendigo Bank services', 'beotravel' ),
                'category' => __( 'Bendigo Bank elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'bendigobank-services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Bendigo Bank services',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_services_html( $atts ) {
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'services1',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="bendigo-service clearfix">
                <div class="bendigo-service-container container">
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="bendigo-single-service">
                            <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                        ?>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
    }
}

new vcBendigoBankServices();
