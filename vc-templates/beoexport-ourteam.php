<?php

class vcBeoexportOurTeam extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_ourteam_mapping' ) );
        add_shortcode( 'vc_beoexport_ourteam', array( $this, 'vc_beoexport_ourteam_html' ) );
    }
    public function vc_beoexport_ourteam_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Our team', 'beotravel' ),
                'base' => 'vc_beoexport_ourteam',
                'description' => __( 'Our team', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'our-team',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Our team',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_ourteam_html( $atts ) {
        $args = array(
        	'post_type' => 'beoexport_ourteam',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
        ?>
        <div class="beoexport-company-team">
            <div class="beoexport-company-team-container container">
                <?php
                while( $query->have_posts() ) :
                    $query->the_post();
                    ?>
                    <div class="beoexport-company-team-individual individual col-xs-12 col-sm-6 col-md-3">
                        <?php the_post_thumbnail( 'beoexport-team', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
    }
}
new vcBeoexportOurTeam();
