<?php

class vcBeogroupServices3 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_services3_mapping' ) );
        add_shortcode( 'vc_beogroup_services3', array( $this, 'vc_beogroup_services3_html' ) );
    }
    public function vc_beogroup_services3_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services 3', 'beogroup' ),
                'base' => 'vc_beogroup_services3',
                'description' => __( 'Services 3', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beogroup' ),
                        'param_name' => 'link6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beogroup' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_services3_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'bgcolor' => '',
                    'text' => '',
                    'image' => '',
                    'title' => '',
                    'subtitle' => '',
                    'link' => '',
                    'image2' => '',
                    'title2' => '',
                    'subtitle2' => '',
                    'link2' => '',
                    'image3' => '',
                    'title3' => '',
                    'subtitle3' => '',
                    'link3' => '',
                    'image4' => '',
                    'title4' => '',
                    'subtitle4' => '',
                    'link4' => '',
                    'image5' => '',
                    'title5' => '',
                    'subtitle5' => '',
                    'link5' => '',
                    'image6' => '',
                    'title6' => '',
                    'subtitle6' => '',
                    'link6' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="about-us">
            <div class="about-us-container container">
                <p><?php echo $text; ?></p>
                <div class="about-sites clearfix">
                    <div class="about-site">
                        <a href="<?php echo $link; ?>"><img src="<?php echo wp_get_attachment_image_src( $image, 'services' )[0]; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>"></a>
                        <a href="<?php echo $link; ?>"><h3><?php echo $title; ?></h3></a>
                        <p><?php echo $subtitle; ?></p>
                    </div>
                    <div class="about-site">
                        <a href="<?php echo $link2; ?>"><img src="<?php echo wp_get_attachment_image_src( $image2, 'services' )[0]; ?>" alt="<?php echo $title2; ?>" title="<?php echo $title2; ?>"></a>
                        <a href="<?php echo $link2; ?>"><h3><?php echo $title2; ?></h3></a>
                        <p><?php echo $subtitle2; ?></p>
                    </div>
                    <div class="about-site">
                        <a href="<?php echo $link3; ?>"><img src="<?php echo wp_get_attachment_image_src( $image3, 'services' )[0]; ?>" alt="<?php echo $title3; ?>" title="<?php echo $title3; ?>"></a>
                        <a href="<?php echo $link3; ?>"><h3><?php echo $title3; ?></h3></a>
                        <p><?php echo $subtitle3; ?></p>
                    </div>
                    <div class="about-site">
                        <a href="<?php echo $link4; ?>"><img src="<?php echo wp_get_attachment_image_src( $image4, 'services' )[0]; ?>" alt="<?php echo $title4; ?>" title="<?php echo $title4; ?>"></a>
                        <a href="<?php echo $link4; ?>"><h3><?php echo $title4; ?></h3></a>
                        <p><?php echo $subtitle4; ?></p>
                    </div>
                    <div class="about-site">
                        <a href="<?php echo $link5; ?>"><img src="<?php echo wp_get_attachment_image_src( $image5, 'services' )[0]; ?>" alt="<?php echo $title5; ?>" title="<?php echo $title5; ?>"></a>
                        <a href="<?php echo $link5; ?>"><h3><?php echo $title5; ?></h3></a>
                        <p><?php echo $subtitle5; ?></p>
                    </div>
                    <div class="about-site">
                        <a href="<?php echo $link6; ?>"><img src="<?php echo wp_get_attachment_image_src( $image6, 'services' )[0]; ?>" alt="<?php echo $title6; ?>" title="<?php echo $title6; ?>"></a>
                        <a href="<?php echo $link6; ?>"><h3><?php echo $title6; ?></h3></a>
                        <p><?php echo $subtitle6; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeogroupServices3();
