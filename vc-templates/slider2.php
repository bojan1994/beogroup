<?php

class vcBeogroupSlider2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_slider2_mapping' ) );
        add_shortcode( 'vc_beogroup_slider2', array( $this, 'vc_beogroup_slider2_html' ) );
    }
    public function vc_beogroup_slider2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Slider bottom', 'beogroup' ),
                'base' => 'vc_beogroup_slider2',
                'description' => __( 'Slider bottom', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider_bottom',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Slider bottom',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_slider2_html( $atts ) {
        $args = array(
        	'post_type' => 'slider_bottom',
            'posts_per_page' => 10,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            $counter = 0;
            ?>
            <div class="slick-carousel">
                <?php
                while( $query->have_posts() ) :
                    $counter++;
                    $query->the_post(); ?>
                        <div class="slick-item">
                            <?php the_post_thumbnail( 'slider-image2', array( 'class' => 'img-responsive', 'alt' => get_the_title() ) ); ?>
                        </div>
            <?php endwhile;
            ?>
        </div>
        <?php
        wp_reset_postdata();
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeogroupSlider2();
