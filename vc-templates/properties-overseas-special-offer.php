<?php

class vcPropertiesOverseasSpecialOffer extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_propertiesoverseas_specialoffer_mapping' ) );
        add_shortcode( 'vc_propertiesoverseas_specialoffer', array( $this, 'vc_propertiesoverseas_specialoffer_html' ) );
    }
    public function vc_propertiesoverseas_specialoffer_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Selected offer', 'beogroup' ),
                'base' => 'vc_propertiesoverseas_specialoffer',
                'description' => __( 'Selected offer', 'beogroup' ),
                'category' => __( 'Properties Overseas elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Selected offer',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Post type',
                    ),
                )
            )
        );
    }
    public function vc_propertiesoverseas_specialoffer_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                ),
                $atts
            )
        );
        $args = array(
            'post_type' => 'special_offer',
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            $id = hash( 'md5', rand( 10, 99 ) . time() );
            $counter = 0;
            ?>
            <div class="selected-offer">
                <div class="selected-offer-container container">
                    <h2><?php echo $title; ?></h2>
                    <div id="<?php echo $id; ?>" class="carousel slide specials-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <?php
                            while( $query->have_posts() ) :
                                $query->the_post();
                                $counter++;
                                ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?> clearfix">
                                    <div class="offer-text ono">
                                        <div class="offer-text-naslov">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                        <div class="offer-info">
                                            <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                            <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                            <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                            <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                            <div class="offer-info-content">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offer-img ono">
                                        <?php the_post_thumbnail( 'real-estate', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                            wp_reset_postdata();
                            ?>
                        </div>
                        <a class="left carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#<?php echo $id; ?>" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        /*
        $args = array(
            'post_type' => 'special_offer',
            'posts_per_page' => 1,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="beotravel-text special-offer">
                <div class="special-offer-container container">
                    <h2><?php echo $title; ?></h2>
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="offer-content clearfix">
                            <div class="offer-text col-xs-12 col-sm-6 col-md-6">
                                <div class="offer-info">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                </div>
                                <?php the_content(); ?>
                            </div>
                            <div class="offer-img col-xs-12 col-sm-6 col-md-6">
                                <?php the_post_thumbnail( 'real-estate', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        */
    }
}

new vcPropertiesOverseasSpecialOffer();
