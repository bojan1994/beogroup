<?php

class vcBeogroupCurrentOffers extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_currentoffers_mapping' ) );
        add_shortcode( 'vc_beogroup_currentoffers', array( $this, 'vc_beogroup_currentoffers_html' ) );
    }
    public function vc_beogroup_currentoffers_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Current offers', 'beogroup' ),
                'base' => 'vc_beogroup_currentoffers',
                'description' => __( 'Current offers', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h2',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Current offers',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_currentoffers_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'subtitle' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="offers" class="container offer-container">
        <div class="offer-heading">
            <h2><?php echo $title; ?></h2>
            <?php
            if( $subtitle ) {
                ?>
                <h4><?php echo $subtitle; ?></h4>
                <?php
            }
            if( $text ) {
                ?>
                <p><?php echo $text; ?></p>
                <?php
            }
            ?>
        </div>
        <?php
        global $post;
        $args = array(
        	'post_type' => 'current_offers',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
                <div class="row">
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="col-sm-6 col-md-3 offer col-xs-12">
                            <?php
                            $image_full = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'full' );
                            $image_thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'current-offer' );
                            ?>
                            <a href="<?php echo $image_full[0]; ?>" rel="lightbox" title="<?php the_title(); ?>">
                                <img src="<?php echo $image_thumbnail[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" style="opacity: 1;width:300px;height:320px;">
                            </a>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            <?php
        else :
            get_template_part( '../content', 'none' );
        endif;
        ?>
        </div>
        <?php
    }
}

new vcBeogroupCurrentOffers();
