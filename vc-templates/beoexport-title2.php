<?php

class vcBeoexportTitle2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_title2_mapping' ) );
        add_shortcode( 'vc_beoexport2_title', array( $this, 'vc_beoexport_title2_html' ) );
    }
    public function vc_beoexport_title2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Title 2', 'beotravel' ),
                'base' => 'vc_beoexport2_title',
                'description' => __( 'Title 2', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_title2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="about-beoexport">
            <div class="about-beoexport-container container">
                <h2><?php echo $title; ?></h2>
                <?php
                if( $text ) {
                    ?>
                    <p><?php echo $text; ?></p>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportTitle2();
