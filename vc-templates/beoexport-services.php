<?php

class vcBeoexportServices extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_services_mapping' ) );
        add_shortcode( 'vc_beoexport_services', array( $this, 'vc_beoexport_services_html' ) );
    }
    public function vc_beoexport_services_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services', 'beotravel' ),
                'base' => 'vc_beoexport_services',
                'description' => __( 'Services', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'url3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_services_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'text' => '',
                    'url' => '',
                    'image2' => '',
                    'title2' => '',
                    'text2' => '',
                    'url2' => '',
                    'image3' => '',
                    'title3' => '',
                    'text3' => '',
                    'url3' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beotravel-team-container send-container container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 individual send-individual">
                    <img src="<?php echo wp_get_attachment_image_src( $image, 'beoexport-services' )[0]; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
                    <h4><?php echo $title; ?></h4>
                    <p><?php echo $text; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 individual send-individual">
                    <img src="<?php echo wp_get_attachment_image_src( $image2, 'beoexport-services' )[0]; ?>" alt="<?php echo $title2; ?>" title="<?php echo $title2; ?>">
                    <h4><?php echo $title2; ?></h4>
                    <p><?php echo $text2; ?></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 individual send-individual">
                    <img src="<?php echo wp_get_attachment_image_src( $image3, 'beoexport-services' )[0]; ?>" alt="<?php echo $title3; ?>" title="<?php echo $title3; ?>">
                    <h4><?php echo $title3; ?></h4>
                    <p><?php echo $text3; ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportServices();
