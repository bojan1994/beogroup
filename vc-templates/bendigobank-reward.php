<?php

class vcBendigoBankReward extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_reward_mapping' ) );
        add_shortcode( 'vc_bendigobank_reward', array( $this, 'vc_bendigobank_reward_html' ) );
    }
    public function vc_bendigobank_reward_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Reward', 'beogroup' ),
                'base' => 'vc_bendigobank_reward',
                'description' => __( 'Reward', 'beogroup' ),
                'category' => __( 'Bendigo Bank elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_reward_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image' => '',
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="bendigo-know liverpool-darker-fff">
            <div class="bendigo-know-container container">
                <div class="bendigo-know-left col-xs-12 col-sm-4 col-md-5">
                    <img src="<?php echo wp_get_attachment_image_src( $image, 'full' )[0]; ?>">
                </div>
                <div class="bendigo-know-right col-xs-12 col-sm-8 col-md-7">
                    <h2><?php echo $title; ?></h2>
                    <p><?php echo $text; ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBendigoBankReward();
