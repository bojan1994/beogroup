<?php

class vcPropertiesOverseasSlider extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_propertiesoverseas_slider_mapping' ) );
        add_shortcode( 'vc_propertiesoverseas_slider', array( $this, 'vc_propertiesoverseas_slider_html' ) );
    }
    public function vc_propertiesoverseas_slider_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Properties Overseas Slider', 'bendigo-bank' ),
                'base' => 'vc_propertiesoverseas_slider',
                'description' => __( 'Properties Overseas Slider', 'bendigo-bank' ),
                'category' => __( 'Properties Overseas elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Image',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Slider',
                    ),
                )
            )
        );
    }
    public function vc_propertiesoverseas_slider_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image2' => '',
                ),
                $atts
            )
        );
        $images = explode( ',', $image2 );
        $args = array(
        	'post_type' => 'slider3',
            'post__in' => $images,
            'orderby' => 'post__in'
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : ?>
            <div id="main-carousel" class="carousel same-carousel overseas-slider section slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                        while( $query->have_posts() ) :
                            $counter++;
                            $query->the_post(); ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                                    <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    <div class="carousel-caption same-carousel-caption">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="carousel-bottom overseas-carousel-botto">
                                        <div class="container carousel-bottom-container">
                                            <div class="white-part clearfix">
                                                <div class="white-heading col-md-12">
                                                    <?php echo get_post_meta( get_the_ID(),'_contact_value_key5',true ); ?>
                                                </div>
                                                <?php
                                                $contact_url = get_post_meta( get_the_ID(),'_contact_url_value_key6',true );
                                                $contact_label = get_post_meta( get_the_ID(),'_contact_value_key6',true );
                                                ?>
                                                <div class="white-part-text">
                                                    <div class="white-part-paragraph col-md-6">
                                                        <?php echo nl2br( get_post_meta( get_the_ID(), '_usluge_value_key2', true ) ); ?>
                                                    </div>
                                                    <div class="white-part-button col-md-5">
                                                        <a class="carousel-button beotravel-carousel-button overseas-carousel-button" href="<?php echo $contact_url; ?>"><?php echo $contact_label; ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    <?php endwhile;
                    wp_reset_postdata();
                    if ( $counter != 1 ) {
                        ?>
                        <ol class="carousel-indicators">
                        <?php
                        for( $i=0; $i<$counter; $i++ ) {
                            ?>
                            <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                            <?php
                        } ?>
                        </ol>
                        <?php
                    } ?>
                </div>
                <?php
                if( $counter != 1 ) {
                    ?>
                    <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <?php
                }
                ?>
            </div>
        <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcPropertiesOverseasSlider();
