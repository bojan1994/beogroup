<?php

class vcBeotravelEvents extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_events_mapping' ) );
        add_shortcode( 'vc_beotravel_events', array( $this, 'vc_beotravel_events_html' ) );
    }
    public function vc_beotravel_events_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Beotravel Events', 'beotravel' ),
                'base' => 'vc_beotravel_events',
                'description' => __( 'Beotravel Events', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Events & Festivals',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beotravel' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_events_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'bgcolor' => '',
                ),
                $atts
            )
        );
        $args = array(
        	'post_type' => 'events',
            'posts_per_page' => 10,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="arrangements-events beotravel-text" style="background-color:<?php echo $bgcolor; ?>">
                <div class="container events-container">
                    <div class="air-fares-text events-text">
                        <h2><?php echo $title; ?></h2>
                        <p><?php echo $text; ?></p>
                    </div>
                    <div class="row">
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="col-md-3 col-xs-12 col-sm-6 single-event">
                                <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                <h4><?php the_title(); ?></h4>
                                <?php the_content(); ?>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
            <?php
            wp_reset_postdata();
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeotravelEvents();
