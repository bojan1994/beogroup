<?php

class vcBeogroupGallery extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_gallery_mapping' ) );
        add_shortcode( 'vc_beogroup_gallery', array( $this, 'vc_beogroup_gallery_html' ) );
    }
    public function vc_beogroup_gallery_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Gallery', 'beogroup' ),
                'base' => 'vc_beogroup_gallery',
                'description' => __( 'Gallery', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'gallery',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Gallery',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_gallery_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="title" class="gallery-container container">
        <div class="gallery-heading">
            <h2><?php echo $title; ?></h2>
        </div>
        <?php
        global $post;
        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
        $args = array(
        	'post_type' => 'gallery',
            'posts_per_page' => '20',
            'paged' => $paged,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="row">
                <?php
                while( $query->have_posts() ) :
                    $query->the_post();
                    ?>
                    <div class="col-sm-6 col-xs-12 col-md-3 gallery-single">
                        <?php the_content(); ?>
                        <h4><?php the_title(); ?></h4>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
            <nav class="pagination" role="navigation">
                <?php
                $big = 999999999;
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, get_query_var( 'paged' ) ),
                    'total' => $query->max_num_pages,
                    'prev_text' => '«',
                    'next_text' => '»',
                ));
                ?>
            </nav>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        ?>
        </div>
        <?php
    }
}

new vcBeogroupGallery();
