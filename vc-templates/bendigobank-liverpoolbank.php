<?php

class vcBendigoBankLiverpoolBank extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_liverpoolbank_mapping' ) );
        add_shortcode( 'vc_bendigobank_liverpoolbank', array( $this, 'vc_bendigobank_liverpoolbank_html' ) );
    }
    public function vc_bendigobank_liverpoolbank_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'About Bendigo Bank Liverpool', 'beogroup' ),
                'base' => 'vc_bendigobank_liverpoolbank',
                'description' => __( 'About Bendigo Bank Liverpool', 'beogroup' ),
                'category' => __( 'Bendigo Bank elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_liverpoolbank_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="liverpool-agency" class="beotravel-text bendigo-liverpool-text">
            <div class="bendigo-liverpool-text-container container">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $text; ?></p>
            </div>
        </div>
        <?php
    }
}

new vcBendigoBankLiverpoolBank();
