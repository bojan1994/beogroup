<?php

class vcBeomediaHome extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beomedia_home_mapping' ) );
        add_shortcode( 'vc_beomedia_home', array( $this, 'vc_beomedia_home_html' ) );
    }
    public function vc_beomedia_home_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Homepage', 'beotravel' ),
                'base' => 'vc_beomedia_home',
                'description' => __( 'Homepage', 'beotravel' ),
                'category' => __( 'Beo Media elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Content',
                    ),
                )
            )
        );
    }
    public function vc_beomedia_home_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'image' => '',
                    'text2' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="media-home">
        <div class="media-home-container container">
        <div class="top clearfix">
        <div class="col-xs-12 col-sm-6 col-md-9 media-text">
        <h2><?php echo $title; ?></h2>
        <p><?php echo $text; ?></p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
        <img src="<?php echo wp_get_attachment_image_src( $image, 'beomedia-home' )[0]; ?>">
        </div>
        </div>
        <div class="bottom">
        <p><?php echo $text2; ?></p>
        </div>
        </div>
        </div>
        <?php
    }
}

new vcBeomediaHome();
