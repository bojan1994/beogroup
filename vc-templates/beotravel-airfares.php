<?php

class vcBeotravelAirFares extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_airfares_mapping' ) );
        add_shortcode( 'vc_beotravel_airfares', array( $this, 'vc_beotravel_airfares_html' ) );
    }
    public function vc_beotravel_airfares_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Air Fares', 'beotravel' ),
                'base' => 'vc_beotravel_airfares',
                'description' => __( 'Air Fares', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'destinations',
                        'heading' => __( 'Destination', 'beotravel' ),
                        'param_name' => 'destination',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'destinations',
                        'heading' => __( 'Destination', 'beotravel' ),
                        'param_name' => 'destination2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_airfares_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'label' => '',
                    'url' => '',
                    'text' => '',
                    'subtitle' => '',
                    'destination' => '',
                    'destination2' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="air" class="air-fares beotravel-text">
            <div class="container air-fares-container">
                <h2><?php echo $title; ?></h2>
                <a class="blue-button" href="<?php echo $url; ?>"><?php echo $label; ?></a>
                <p class="air-fares-text"><?php echo $text; ?></p>
                <a href="<?php echo $url; ?>"><h3><?php echo $subtitle; ?></h3></a>
                <div class="air-fares-bottom-text clearfix">
                    <div class="left">
                        <p><i class="fas fa-check"></i> <?php echo $destination; ?></p>
                    </div>
                    <div class="right">
                        <p><i class="fas fa-check"></i> <?php echo $destination2; ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeotravelAirFares();
