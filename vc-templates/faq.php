<?php

class vcBeogroupFaq extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_faq_mapping' ) );
        add_shortcode( 'vc_beogroup_faq', array( $this, 'vc_beogroup_faq_html' ) );
    }
    public function vc_beogroup_faq_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'FAQ', 'beogroup' ),
                'base' => 'vc_beogroup_faq',
                'description' => __( 'FAQ', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'faq',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'FAQ',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_faq_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'subtitle' => 'faq',
                ),
                $atts
            )
        );
        ?>
        <div id="faq" class="faq-container container">
        <div class="faq-heading">
            <h2><?php echo $title; ?></h2>
            <h4><?php echo $subtitle; ?></h4>
        </div>
        <?php
        $args1 = array(
            'post_type' => '',
            'tax_query' => array(
                array(
                    'taxonomy' => 'faq_category',
                    'field' => 'slug',
                    'terms' => 'beo-export',
                    'operator' => 'IN'
                )
            ),
            'posts_per_page' => 100
        );
        $query1 = new WP_Query( $args1 );
        if( $query1->have_posts() ) :
            ?>
            <div class="question-block">
            <h5>BEO-Export</h5>
            <?php
            while( $query1->have_posts() ) :
                $query1->the_post();
                ?>
                <div class="question-block-inner">
                    <h6 class="question"><?php the_title(); ?></h6>
                    <div class="answer">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        $args2 = array(
            'post_type' => '',
            'tax_query' => array(
                array(
                    'taxonomy' => 'faq_category',
                    'field' => 'slug',
                    'terms' => 'beo-travel',
                    'operator' => 'IN'
                )
            ),
        );
        $query2 = new WP_Query( $args2 );
        if( $query2->have_posts() ) :
            ?>
            <div class="question-block">
            <h5>BEO-Travel</h5>
            <?php
            while( $query2->have_posts() ) :
                $query2->the_post();
                ?>
                <div class="question-block-inner">
                    <h6 class="question"><?php the_title(); ?></h6>
                    <div class="answer">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        $args3 = array(
            'post_type' => '',
            'tax_query' => array(
                array(
                    'taxonomy' => 'faq_category',
                    'field' => 'slug',
                    'terms' => 'bendigo-bank',
                    'operator' => 'IN'
                )
            ),
        );
        $query3 = new WP_Query( $args3 );
        if( $query3->have_posts() ) :
            ?>
            <div class="question-block">
            <h5>Bendigo Bank</h5>
            <?php
            while( $query3->have_posts() ) :
                $query3->the_post();
                ?>
                <div class="question-block-inner">
                    <h6 class="question"><?php the_title(); ?></h6>
                    <div class="answer">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        $args4 = array(
            'post_type' => '',
            'tax_query' => array(
                array(
                    'taxonomy' => 'faq_category',
                    'field' => 'slug',
                    'terms' => 'real-estate',
                    'operator' => 'IN'
                )
            ),
        );
        $query4 = new WP_Query( $args4 );
        if( $query4->have_posts() ) :
            ?>
            <div class="question-block">
            <h5>Real-estate</h5>
            <?php
            while( $query4->have_posts() ) :
                $query4->the_post();
                ?>
                <div class="question-block-inner">
                    <h6 class="question"><?php the_title(); ?></h6>
                    <div class="answer">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php
            endwhile;
            wp_reset_postdata();
            ?>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
        ?>
        </div>
        <?php
    }
}

new vcBeogroupFaq();
