<?php

class vcBeotravelReferences extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_references_mapping' ) );
        add_shortcode( 'vc_beotravel_references', array( $this, 'vc_beotravel_references_html' ) );
    }
    public function vc_beotravel_references_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'References', 'beotravel' ),
                'base' => 'vc_beotravel_references',
                'description' => __( 'References', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'references',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'References',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_references_html( $atts ) {
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'references',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="beotravel-references-container container">
                <div class="row">
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-3 references-image">
                            <?php the_post_thumbnail( 'beotravel-references', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                        </div>
                        <?php
                    endwhile;
                    ?>
                </div>
            </div>
            <?php
            wp_reset_postdata();
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
        return $html;
    }
}

new vcBeotravelReferences();
