<?php



class vcPropertiesOverseasSearch extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_propertiesoverseas_search_mapping' ) );
        add_shortcode( 'vc_propertiesoverseas_search', array( $this, 'vc_propertiesoverseas_search_html' ) );
    }
    public function vc_propertiesoverseas_search_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Search', 'beotravel' ),
                'base' => 'vc_propertiesoverseas_search',
                'description' => __( 'Search', 'beotravel' ),
                'category' => __( 'Properties Overseas elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Search',
                    ),
                )
            )
        );
    }
    public function vc_propertiesoverseas_search_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="search" class="real-estate">
            <div class="real-estate-container container">
            <h2><?php echo $title; ?></h2>
            <form class="real-estate-form clearfix" action="#search" method="get">
                <div class="seling-reting ovo">
                <?php
                $terms2 = get_terms( 'type', array(
                    'hide_empty' => false,
                ) );
                foreach( $terms2 as $term2 ) {
                    ?>
                    <div class="checkbox-inline custom-controls-stacked d-block my-3">
                        <label class="custom-control-label custom-control custom-checkbox" for="<?php echo $term2->slug; ?>"><?php echo $term2->name; ?>
                            <input id="<?php echo $term2->slug; ?>" type="checkbox" class="checkbox custom-control-input" name="type" value="<?php echo $term2->slug; ?>">
                            <span class="custom-control-indicator"></span>
    			            <span class="custom-control-description"></span>
                        </label>
                    </div>
                    <?php
                }
                ?>
            </div>
                <div class="property-type ovo">
                    <label for="property">Property Type</label>
                    <select id="property" name="property" class="form-control">
                        <option value="" disabled selected></option>
                        <?php
                        $terms = get_terms( 'property', array(
                            'hide_empty' => false,
                        ) );
                        foreach( $terms as $term ) {
                            ?>
                            <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="budget ovo">
                    <p>Budget</p>
                    <input type="text" name="budget_min" placeholder="min">
                    <input type="text" name="budget_max" placeholder="max">
                </div>
                <div class="search-button ovo">
                    <input type="submit" name="btn_submit" value="Search">
                </div>
            </form>
            <div class="real-estate-content">
                <div class="sort">
                    <a href="?orderby=time#search">Sort by date</a>
                    <a href="?orderby=price#search">Sort by price</a>
                </div>
                <?php
                if( isset( $_GET['btn_submit'] ) ) {
                    if( isset( $_GET['type'] ) && ! isset( $_GET['property'] ) && empty( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                        $args = array(
                            'post_type' => 'special_offer',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'type',
                                    'field' => 'slug',
                                    'terms' => $_GET['type'],
                                    'operator' => 'IN'
                                )
                            ),
                            'posts_per_page' => 3,
                            'paged' => $paged,
                        );
                    } elseif( isset( $_GET['property'] ) && ! isset( $_GET['type'] ) && empty( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                        $args = array(
                            'post_type' => 'special_offer',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'property',
                                    'field' => 'slug',
                                    'terms' => $_GET['property'],
                                    'operator' => 'IN'
                                )
                            ),
                            'posts_per_page' => 3,
                            'paged' => $paged,
                        );
                    } elseif( isset( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) && ! isset( $_GET['property'] ) && ! isset( $_GET['type'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval( $_GET['budget_min'] ),
                                        'type' => 'NUMERIC',
                                        'compare' => '>'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['budget_max'] ) && empty( $_GET['budget_min'] ) && ! isset( $_GET['property'] ) && ! isset( $_GET['type'] ) ) {
                        if( is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval( $_GET['budget_max'] ),
                                        'type' => 'NUMERIC',
                                        'compare' => '<'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) && ! isset( $_GET['property'] ) && ! isset( $_GET['type'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) && is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => array( intval($_GET['budget_min']), intval($_GET['budget_max']) ),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && isset( $_GET['property'] ) && empty( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                        $args = array(
                            'post_type' => 'special_offer',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'type',
                                    'field' => 'slug',
                                    'terms' => $_GET['type'],
                                    'operator' => 'IN'
                                ),
                                array(
                                    'taxonomy' => 'property',
                                    'field' => 'slug',
                                    'terms' => $_GET['property'],
                                    'operator' => 'IN'
                                )
                            ),
                            'posts_per_page' => 3,
                            'paged' => $paged,
                        );
                    } elseif( isset( $_GET['type'] ) && isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_min']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && isset( $_GET['property'] ) && isset( $_GET['budget_max'] ) && empty( $_GET['budget_min'] ) ) {
                        if( is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_max']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) && is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        //'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => array( intval($_GET['budget_min']), intval($_GET['budget_max']) ),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && ! isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_min']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && ! isset( $_GET['property'] ) && empty( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_max']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( isset( $_GET['type'] ) && ! isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) && is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'type',
                                        'field' => 'slug',
                                        'terms' => $_GET['type'],
                                        'operator' => 'IN'
                                    ),
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => array( intval($_GET['budget_min']), intval($_GET['budget_max']) ),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( ! isset( $_GET['type'] ) && isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && empty( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_min']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( ! isset( $_GET['type'] ) && isset( $_GET['property'] ) && empty( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => intval($_GET['budget_max']),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    } elseif( ! isset( $_GET['type'] ) && isset( $_GET['property'] ) && isset( $_GET['budget_min'] ) && isset( $_GET['budget_max'] ) ) {
                        if( is_numeric( $_GET['budget_min'] ) && is_numeric( $_GET['budget_max'] ) ) {
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'special_offer',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'property',
                                        'field' => 'slug',
                                        'terms' => $_GET['property'],
                                        'operator' => 'IN'
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => '_contact_value_key8',
                                        'value' => array( intval($_GET['budget_min']), intval($_GET['budget_max']) ),
                                        'type' => 'NUMERIC',
                                        'compare' => 'BETWEEN'
                                    )
                                ),
                                'posts_per_page' => 3,
                                'paged' => $paged,
                            );
                        } else {
                            echo '<script>alert("Only numbers are allowed in budget")</script>';
                        }
                    }
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        if( $_GET['type'] != null && $_GET['property'] == null && $_GET['budget_min'] == null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['type']; ?></p>
                            <?php
                        }
                        if( $_GET['property'] != null && $_GET['type'] == null && $_GET['budget_min'] == null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?></p>
                            <?php
                        }
                        if( $_GET['budget_min'] != null && $_GET['budget_max'] == null && $_GET['type'] == null && $_GET['property'] == null ) {
                            ?>
                            <p>Search criteria: minimun budget: <?php echo $_GET['budget_min']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['budget_max'] != null && $_GET['budget_min'] == null && $_GET['type'] == null && $_GET['property'] == null ) {
                            ?>
                            <p>Search criteria: maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['budget_min'] != null && $_GET['budget_max'] != null && $_GET['type'] == null && $_GET['property'] == null ) {
                            ?>
                            <p>Search criteria: minimum budget: <?php echo $_GET['budget_min']; ?> &euro;, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['property'] != null && $_GET['budget_min'] == null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, <?php echo $_GET['type']; ?></p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['property'] != null && $_GET['budget_min'] != null && $_GET['budget_max'] != null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, <?php echo $_GET['type']; ?>, minimum budget: <?php echo $_GET['budget_min']; ?> &euro;, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['budget_min'] != null && $_GET['property'] == null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['type']; ?>, minimun budget: <?php echo $_GET['budget_min']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['budget_max'] != null && $_GET['property'] == null && $_GET['budget_min'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['type']; ?>, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['budget_min'] != null && $_GET['budget_max'] != null && $_GET['property'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['type']; ?>, minimun budget: <?php echo $_GET['budget_min']; ?> &euro;, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['property'] != null && $_GET['budget_min'] != null && $_GET['type'] == null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, minimun budget: <?php echo $_GET['budget_min']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['property'] != null && $_GET['budget_max'] != null && $_GET['type'] == null && $_GET['budget_min'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['property'] != null && $_GET['budget_min'] != null && $_GET['budget_max'] != null && $_GET['type'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, minimun budget: <?php echo $_GET['budget_min']; ?> &euro;, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['property'] != null && $_GET['budget_min'] != null && $_GET['budget_max'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, <?php echo $_GET['type']; ?>, minimun budget: <?php echo $_GET['budget_min']; ?> &euro;</p>
                            <?php
                        }
                        if( $_GET['type'] != null && $_GET['property'] != null && $_GET['budget_max'] != null && $_GET['budget_min'] == null ) {
                            ?>
                            <p>Search criteria: <?php echo $_GET['property']; ?>, <?php echo $_GET['type']; ?>, maximum budget: <?php echo $_GET['budget_max']; ?> &euro;</p>
                            <?php
                        }
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="real-estate-single clearfix">
                                <div class="real-estate-single-img">
                                    <p><?php echo do_shortcode( wpautop( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ) ); ?></p>
                                </div>
                                <div class="real-estate-single-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                    <div class="content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                        <div class="pagination-nav">
                            <?php
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var( 'paged' ) ),
                                'total' => $query->max_num_pages,
                                'prev_text' => __( '«' ),
                                'next_text' => __( '»' ),
                            ));
                            ?>
                        </div>
                        <?php
                        wp_reset_postdata();
                    else :
                        _e( 'Sorry, no content found', 'beogroup' );
                    endif;
                } elseif( isset($_GET['orderby']) && $_GET['orderby'] == 'time' ) {
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'post_type' => 'special_offer',
                        'order' => 'ASC',
                        'orderby' => 'date',
                        'posts_per_page' => 3,
                        'paged' => $paged,
                    );
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        ?>
                        <p>Sorted by date</p>
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="real-estate-single clearfix">
                                <div class="real-estate-single-img">
                                    <p><?php echo do_shortcode( wpautop( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ) ); ?></p>
                                </div>
                                <div class="real-estate-single-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                    <div class="content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                        <div class="pagination-nav">
                            <?php
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var( 'paged' ) ),
                                'total' => $query->max_num_pages,
                                'prev_text' => __( '«' ),
                                'next_text' => __( '»' ),
                            ));
                            ?>
                        </div>
                        <?php
                        wp_reset_postdata();
                    else :
                        _e( 'Sorry, no content found', 'beogroup' );
                    endif;
                } elseif( isset($_GET['orderby']) && $_GET['orderby'] == 'price' ) {
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'post_type' => 'special_offer',
                        'order' => 'DESC',
                        'meta_key' => '_contact_value_key8',
                        'orderby' => 'meta_value_num',
                        'posts_per_page' => 3,
                        'paged' => $paged,
                    );
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        ?>
                        <p>Sorted by price</p>
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="real-estate-single clearfix">
                                <div class="real-estate-single-img">
                                    <p><?php echo do_shortcode( wpautop( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ) ); ?></p>
                                </div>
                                <div class="real-estate-single-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                    <div class="content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                        <div class="pagination-nav">
                            <?php
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var( 'paged' ) ),
                                'total' => $query->max_num_pages,
                                'prev_text' => __( '«' ),
                                'next_text' => __( '»' ),
                            ));
                            ?>
                        </div>
                        <?php
                        wp_reset_postdata();
                    else :
                        _e( 'Sorry, no content found', 'beogroup' );
                    endif;
                } elseif( isset($_GET['orderby']) && $_GET['orderby'] == 'popularity' ) {
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'post_type' => 'special_offer',
                        'order' => 'DESC',
                        'meta_key' => 'post_views_count',
                        'orderby' => 'meta_value_num',
                        'posts_per_page' => 3,
                        'paged' => $paged,
                    );
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="real-estate-single clearfix">
                                <div class="real-estate-single-img">
                                    <p><?php echo do_shortcode( wpautop( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ) ); ?></p>
                                </div>
                                <div class="real-estate-single-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                    <div class="content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                        <div class="pagination-nav">
                            <?php
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var( 'paged' ) ),
                                'total' => $query->max_num_pages,
                                'prev_text' => __( '«' ),
                                'next_text' => __( '»' ),
                            ));
                            ?>
                        </div>
                        <?php
                        wp_reset_postdata();
                    else :
                        _e( 'Sorry, no content found', 'beogroup' );
                    endif;
                } else {
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $args = array(
                        'post_type' => 'special_offer',
                        'posts_per_page' => 3,
                        'paged' => $paged,
                    );
                    $query = new WP_Query( $args );
                    if( $query->have_posts() ) :
                        while( $query->have_posts() ) :
                            $query->the_post();
                             ?>
                            <div class="real-estate-single clearfix">
                                <div class="real-estate-single-img">
                                    <p><?php echo do_shortcode( wpautop( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ) ); ?></p>
                                </div>
                                <div class="real-estate-single-text">
                                    <h3><?php the_title(); ?></h3>
                                    <p class="address"><?php echo get_post_meta( get_the_ID(),'_contact_value_key7',true ); ?></p>
                                    <p class="price">Price: &euro; <?php echo get_post_meta( get_the_ID(),'_contact_value_key8',true ); ?></p>
                                    <p class="space">Living place: <?php echo get_post_meta( get_the_ID(),'_contact_value_key11',true ); ?> m<sup>2</sup></p>
                                    <p class="land">Land space: <?php echo get_post_meta( get_the_ID(),'_contact_value_key9',true ); ?> m<sup>2</sup></p>
                                    <div class="content">
                                        <?php the_content(); ?>
                                            <div class="search-modal-button">
                                                <a class="beotravel-carousel-button" href="#" data-toggle="modal" data-target="#bojanov-modal" data-property="<?php the_title(); ?>" onClick="document.getElementById('nevidljivi').value=this.dataset.property">More info</a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                        <div class="pagination-nav">
                            <?php
                            $big = 999999999;
                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var( 'paged' ) ),
                                'total' => $query->max_num_pages,
                                'prev_text' => __( '«' ),
                                'next_text' => __( '»' ),
                            ));
                            ?>
                        </div>
                        <?php
                        wp_reset_postdata();
                    else :
                        _e( 'Sorry, no content found', 'beogroup' );
                    endif;
                }
                ?>
                <div class="search-modal">
                    <div class="search-form">
                        <div class="modal fade" id="bojanov-modal" tabindex="-1" role="dialog" aria-labelledby="bojanov-modallabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <form id="bojanova-forma" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post">
                                        <div class="name">
                                            <label for="name">Name</label> <br>
                                            <input id="name" type="text" name="name_"> <br>
                                        </div>
                                        <div class="email">
                                            <label for="email">E-mail</label> <br>
                                            <input id="email" type="text" name="email"> <br>
                                        </div>
                                        <div class="phone">
                                            <label for="phone">Phone number</label> <br>
                                            <input id="phone" type="text" name="phone"> <br>
                                        </div>
                                        <div class="msg">
                                            <label for="message">Message</label> <br>
                                            <textarea id="message" name="message" rows="8" cols="80"></textarea>
                                        </div>
                                        <input id="nevidljivi" type="hidden" name="property" value="">
                                        <input type="submit" class="btn btn-primary" name="btn_submit2" value="Send">
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <?php
    }
}

new vcPropertiesOverseasSearch();
