<?php

class vcBeoexportTitle extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_title_mapping' ) );
        add_shortcode( 'vc_beoexport_title', array( $this, 'vc_beoexport_title_html' ) );
    }
    public function vc_beoexport_title_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Title', 'beotravel' ),
                'base' => 'vc_beoexport_title',
                'description' => __( 'Title', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_title_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="title" class="send-money">
            <div class="send-money-container container">
                <h2><?php echo $title; ?></h2>
                <?php
                if( $text ) {
                    ?>
                    <p><?php echo $text; ?></p>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportTitle();
