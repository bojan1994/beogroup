<?php

class vcBeoexportCalculator extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_calculator_mapping' ) );
        add_shortcode( 'vc_beoexport_calculator', array( $this, 'vc_beoexport_calculator_html' ) );
    }
    public function vc_beoexport_calculator_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Calculator', 'beotravel' ),
                'base' => 'vc_beoexport_calculator',
                'description' => __( 'Calculator', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                )
            )
        );
    }
    public function vc_beoexport_calculator_html() {
        ?>
        
        <?php
    }
}

new vcBeoexportCalculator();
