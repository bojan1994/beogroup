<?php

class vcBeoexportText2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_text2_mapping' ) );
        add_shortcode( 'vc_beoexport_text2', array( $this, 'vc_beoexport_text2_html' ) );
    }
    public function vc_beoexport_text2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Text 2', 'beotravel' ),
                'base' => 'vc_beoexport_text2',
                'description' => __( 'Text 2', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_text2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'text' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="container money-transfer-container">
            <p><?php echo $text; ?></p>
        </div>
        <?php
    }
}

new vcBeoexportText2();
