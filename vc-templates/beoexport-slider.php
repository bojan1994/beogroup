<?php

class vcBeoExportSlider extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_slider_mapping' ) );
        add_shortcode( 'vc_beoexport_slider', array( $this, 'vc_beoexport_slider_html' ) );
    }
    public function vc_beoexport_slider_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Beo Export Slider', 'bendigo-bank' ),
                'base' => 'vc_beoexport_slider',
                'description' => __( 'Beo Export Slider', 'bendigo-bank' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Image',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Slider',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_slider_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'image2' => '',
                ),
                $atts
            )
        );
        $images = explode( ',', $image2 );
        $args = array(
        	'post_type' => 'slider5',
            'post__in' => $images,
            'orderby' => 'post__in',
            'posts_per_page' => 20
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : ?>
            <div id="main-carousel" class="carousel beoexport-slider beotravel-slider same-carousel bendigo-slider section slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $counter = 0;
                        while( $query->have_posts() ) :
                            $counter++;
                            $query->the_post(); ?>
                                <div class="item<?php echo ( $counter == 1 ) ? ' active' : ''; ?>">
                                    <?php the_post_thumbnail( 'slider-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    <div class="carousel-caption same-carousel-caption">
                                        <h1><?php the_title(); ?></h1>
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                    <?php endwhile;
                    wp_reset_postdata();
                    if ( $counter != 1 ) {
                        ?>
                        <ol class="carousel-indicators">
                        <?php
                        for( $i=0; $i<$counter; $i++ ) {
                            ?>
                            <li data-target="#main-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i == 0) ? 'active' : ''; ?>"></li>
                            <?php
                        } ?>
                        </ol>
                        <?php
                    } ?>
                    <div class="carousel-bottom beoexport-carousel-bottom">
                        <div class="carousel-bottom-container container beoexport-carousel-bottom-container">
                            <div class="calculator">
                            <h4>Send Money Online</h4>
        <form class="clearfix" id="form_slanje" method="POST" action="" onkeypress="return event.keyCode != 13;" enctype="multipart/form-data">
            <div class="amount col-xs-12 col-sm-4 col-md-4">
                <p>You transfer Amount</p>
                <div class="amount-wrapper">
                    <label for="iznos_uplate">AUD</label>
                    <input type="text" class="form-control" name="iznos_uplate" id="iznos_uplate" maxlength="7" autocomplete="off" placeholder="$ 0">
                </div>
            </div>
            <div class="gets col-xs-12 col-sm-4 col-md-4">
                <p>Recipient gets*</p>
                <div class="gets-wrapper">
                    <label for="iznos_isplate">EUR</label>
                    <input type="text" class="form-control" name="iznos_isplate" id="iznos_isplate" maxlength="7" autocomplete="off" placeholder="€ 0">
                </div>
            </div>
            <p>
                <span>
                    <option name="odnos_valuta" id="odnos_valuta" style="display:none" value="<?php echo get_theme_mod( 'calculator_value' ); ?>|EUR|AUD/EUR" send=""> AUD/EUR SEND </option>
                </span>
            </p>
            <div class="beoexport-carousel-button clearfix col-xs-12 col-sm-4 col-md-4">
                <a class="beoexport-carousel-link" href="https://beoexport.com.au/BeoExport/login.php" target="_blank">Send Money Now</a>
            </div>
        </form>
        <p class="transfer-info">* Transfer fee not calculated in price. Payout amount will be rounded to the nearest 5 Euro. See <a href="https://starconfighost.com.au/#footer" target="_blank">Terms &amp; Policies</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if( $counter != 1 ) {
                    ?>
                    <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <?php
                }
                ?>
            </div>
        <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeoExportSlider();
