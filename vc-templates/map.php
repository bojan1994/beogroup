<?php

class vcBeogroupMap extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_map_mapping' ) );
        add_shortcode( 'vc_beogroup_map', array( $this, 'vc_beogroup_map_html' ) );
    }
    public function vc_beogroup_map_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Map', 'beogroup' ),
                'base' => 'vc_beogroup_map',
                'description' => __( 'Map', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Map',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Map',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'latitude',
                        'heading' => __( 'Latitude', 'ecotech' ),
                        'param_name' => 'latitude',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Map',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'longitude',
                        'heading' => __( 'Longitude', 'ecotech' ),
                        'param_name' => 'longitude',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Map',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Export Australia',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Export Australia',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Export Australia',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Export Australia',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Travel',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Travel',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Travel',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Travel',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Radio',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Radio',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Radio',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-Radio',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bendigo Bank Liverpool',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bendigo Bank Liverpool',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bendigo Bank Liverpool',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Bendigo Bank Liverpool',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Properties Overseas',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Properties Overseas',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Properties Overseas',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Properties Overseas',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Vesti Online',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Vesti Online',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Vesti Online',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Vesti Online',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'name',
                        'heading' => __( 'Name', 'beogroup' ),
                        'param_name' => 'name123',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-GROUP',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'phone',
                        'heading' => __( 'Phone', 'beogroup' ),
                        'param_name' => 'phone123',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-GROUP',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'fax',
                        'heading' => __( 'Mobile', 'beogroup' ),
                        'param_name' => 'fax123',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-GROUP',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'suburb',
                        'heading' => __( 'Email', 'beogroup' ),
                        'param_name' => 'suburb123',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'BEO-GROUP',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title2',
                        'heading' => __( 'Form Title', 'beogroup' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Map',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_map_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'subtitle' => '',
                    'latitude' => '',
                    'longitude' => '',
                    'name' => '',
                    'phone' => '',
                    'fax' => '',
                    'suburb' => '',
                    'name1' => '',
                    'phone1' => '',
                    'fax1' => '',
                    'suburb1' => '',
                    'name2' => '',
                    'phone2' => '',
                    'fax2' => '',
                    'suburb2' => '',
                    'name3' => '',
                    'phone3' => '',
                    'fax3' => '',
                    'suburb3' => '',
                    'name4' => '',
                    'phone4' => '',
                    'fax4' => '',
                    'suburb4' => '',
                    'name5' => '',
                    'phone5' => '',
                    'fax5' => '',
                    'suburb5' => '',
                    'title2' => '',
                    'name123' => '',
                    'phone123' => '',
                    'fax123' => '',
                    'suburb123' => '',
                ),
                $atts
            )
        );
        ?>
        <div id="contact" class="map">
            <div class="map-heading">
                <h2><?php echo $title; ?></h2>
                <h4><?php echo $subtitle; ?></h4>
            </div>
            <div class="map-section clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 map-map">
                    <div id="map" style="position: relative; overflow: hidden; width:1920px; height:1000px;"></div>
                    <script>
                        function initMap(){
                            var options = {
                                zoom:15,
                                center:{lat:<?php echo $latitude ?> ,lng:<?php echo $longitude ?>}
                            };
                            var map = new google.maps.Map(document.getElementById('map'),options);
                            var marker = new google.maps.Marker({
                                position:{lat:<?php echo $latitude ?> ,lng:<?php echo $longitude ?>},
                                map:map
                            });
                            var infoWindow = new google.maps.InfoWindow({
                                content:'<h3>BEO-Group 68 Moore st, Liverpool NSW 2170, Australia</h3>'
                            });
                            marker.addListener('click',function(){
                                infoWindow.open(map,marker);
                            });
                        }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBGwFyGCyj-ZokTR20y_pZ6ng-AtI-7E4&callback=initMap"
                    async defer></script>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 map-info informations-container">
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Mobile</th>
                            <th>E-mail</th>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name123; ?></td>
                            <td><?php echo $phone123; ?></td>
                            <td><?php echo $fax123; ?></td>
                            <td><a href="mailto:<?php echo $suburb123; ?>"><?php echo $suburb123; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name; ?></td>
                            <td><?php echo $phone; ?></td>
                            <td><?php echo $fax; ?></td>
                            <td><a href="mailto:<?php echo $suburb; ?>"><?php echo $suburb; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name1; ?></td>
                            <td><?php echo $phone1; ?></td>
                            <td><?php echo $fax1; ?></td>
                            <td><a href="mailto:<?php echo $suburb1; ?>"><?php echo $suburb1; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name3; ?></td>
                            <td><?php echo $phone3; ?></td>
                            <td><?php echo $fax3; ?></td>
                            <td><a href="mailto:<?php echo $suburb3; ?>"><?php echo $suburb3; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name4; ?></td>
                            <td><?php echo $phone4; ?></td>
                            <td><?php echo $fax4; ?></td>
                            <td><a href="mailto:<?php echo $suburb4; ?>"><?php echo $suburb4; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name2; ?></td>
                            <td><?php echo $phone2; ?></td>
                            <td><?php echo $fax2; ?></td>
                            <td><a href="mailto:<?php echo $suburb2; ?>"><?php echo $suburb2; ?></a></td>
                        </tr>
                        <tr>
                            <td class="name"><?php echo $name5; ?></td>
                            <td><?php echo $phone5; ?></td>
                            <td><?php echo $fax5; ?></td>
                            <td><a href="mailto:<?php echo $suburb5; ?>"><?php echo $suburb5; ?></a></td>
                        </tr>
                    </table>
                    <div class="form-container">
                        <h2><?php echo $title2; ?></h2>
                        <form action="#" method="post">
                            <div class="name form-info">
                                <label for="fullname">Your name</label> <br>
                                <input type="text" name="fullname" id="fullname"> <br>
                            </div>
                            <div class="email form-info">
                                <label for="email">E-mail</label> <br>
                                <input type="text" name="email" id="email"> <br>
                            </div>
                            <div class="subject form-info">
                                <label for="subject">Subject</label> <br>
                                <input type="text" name="subject" id="subject"> <br>
                            </div>
                            <div class="message form-info">
                                <label for="message">Your message</label> <br>
                                <textarea name="message" rows="8" cols="80" id="message"></textarea> <br>
                            </div>
                            <div class="g-recaptcha" data-sitekey="6Ld0GW0UAAAAAE_jvMF7ShftHDuhXLrWKlbHKvZi"></div>
                            <button class="blue-button" type="submit" name="btn_submit">Send</button>
                            <?php
                            require __DIR__ . '/../inc/contactform.php';
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeogroupMap();
