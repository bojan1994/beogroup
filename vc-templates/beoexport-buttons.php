<?php

class vcBeoexportButtons extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoexport_buttons_mapping' ) );
        add_shortcode( 'vc_beoexport_buttons', array( $this, 'vc_beoexport_buttons_html' ) );
    }
    public function vc_beoexport_buttons_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Buttons', 'beotravel' ),
                'base' => 'vc_beoexport_buttons',
                'description' => __( 'Buttons', 'beotravel' ),
                'category' => __( 'Beo Export elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beotravel' ),
                        'param_name' => 'label2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beotravel' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Button 2',
                    ),
                )
            )
        );
    }
    public function vc_beoexport_buttons_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'label' => '',
                    'url' => '',
                    'label2' => '',
                    'url2' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="send-money-buttons">
            <div class="send-money-buttons-container container">
                <div class="left-button col-xs-12 col-sm-6 col-md-6 clearfix">
                    <a href="<?php echo $url; ?>" target="_blank"><?php echo $label; ?></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 right-button clearfix">
                    <a href="<?php echo $url2; ?>" target="_blank"><?php echo $label2; ?></a>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoexportButtons();
