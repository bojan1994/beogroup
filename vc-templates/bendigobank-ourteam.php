<?php

class vcBendigoBankOurTeam extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_bendigobank_ourteam_mapping' ) );
        add_shortcode( 'vc_bendigobank_ourteam', array( $this, 'vc_bendigobank_ourteam_html' ) );
    }
    public function vc_bendigobank_ourteam_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Our team', 'beotravel' ),
                'base' => 'vc_bendigobank_ourteam',
                'description' => __( 'Our team', 'beotravel' ),
                'category' => __( 'Bendigo Bank elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'our-team',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Our team',
                    ),
                )
            )
        );
    }
    public function vc_bendigobank_ourteam_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                ),
                $atts
            )
        );
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'bendigobank_ourteam',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="bendigo-team beotravel-text">
                <div class="container bendigo-team-container">
                    <div class="row">
                        <h2><?php echo $title; ?></h2>
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="bendigo-team-single col-xs-12 col-sm-6 col-md-3">
                                <div class="single-bendigo">
                                    <?php the_post_thumbnail( 'beotravel-team', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    <h4><?php the_title(); ?></h4>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
    }
}

new vcBendigoBankOurTeam();
