<?php

class vcBeoradioOurTeam extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoradio_ourteam_mapping' ) );
        add_shortcode( 'vc_beoradio_ourteam', array( $this, 'vc_beoradio_ourteam_html' ) );
    }
    public function vc_beoradio_ourteam_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Our team', 'beotravel' ),
                'base' => 'vc_beoradio_ourteam',
                'description' => __( 'Our team', 'beotravel' ),
                'category' => __( 'Beo Radio elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'our-team',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Our team',
                    ),
                )
            )
        );
    }
    public function vc_beoradio_ourteam_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                ),
                $atts
            )
        );
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'beoradio_our_team',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="our-team-section beotravel-text">
                <div class="container our-team-container">
                    <h2><?php echo $title ?></h2>
                    <div class="row">
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="member col-xs-12 col-md-6">
                                <?php the_post_thumbnail( 'beotravel-team', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                <h4><?php the_title(); ?></h4>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                    <div class="our-team-text">
                        <p><?php echo $text; ?></p>
                    </div>
                </div>
            </div>
        <?php
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
    }
}

new vcBeoradioOurTeam();
