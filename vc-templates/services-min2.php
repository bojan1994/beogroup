<?php

class vcBeogroupServicesMin2 extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_servicesmin2_mapping' ) );
        add_shortcode( 'vc_beogroup_servicesmin2', array( $this, 'vc_beogroup_servicesmin2_html' ) );
    }
    public function vc_beogroup_servicesmin2_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services short 2', 'beogroup' ),
                'base' => 'vc_beogroup_servicesmin2',
                'description' => __( 'Services short 2', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h2',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beogroup' ),
                        'param_name' => 'label',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beogroup' ),
                        'param_name' => 'url',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beogroup' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beogroup' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'h2',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beogroup' ),
                        'param_name' => 'text2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beogroup' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'label',
                        'heading' => __( 'Label', 'beogroup' ),
                        'param_name' => 'label2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'url',
                        'heading' => __( 'Url', 'beogroup' ),
                        'param_name' => 'url2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beogroup' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_servicesmin2_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'subtitle' => '',
                    'text' => '',
                    'image' => '',
                    'label' => '',
                    'url' => '',
                    'title2' => '',
                    'subtitle2' => '',
                    'text2' => '',
                    'image2' => '',
                    'label2' => '',
                    'url2' => '',
                    'bgcolor' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beogroup-options">
            <div class="container options-container">
                <div class="row">
                    <a href="<?php echo $url; ?>">
                        <div class="col-md-6 option clearfix">
                            <div class="option-text disco">
                                <h3><?php echo $title; ?></h3>
                                <h4><?php echo $subtitle; ?></h4>
                                <p><?php echo $text; ?></p>
                            </div>
                            <div class="option-search disco">
                                <img src="<?php echo wp_get_attachment_image_src( $image, 'services-min2' )[0] ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
                                <?php echo $label; ?>
                            </div>
                        </div>
                    </a>
                    <a href="<?php echo $url2; ?>">
                        <div class="col-md-6 option clearfix">
                            <div class="option-text disco">
                                <h3><?php echo $title2; ?></h3>
                                <h4><?php echo $subtitle2; ?></h4>
                                <p><?php echo $text2; ?></p>
                            </div>
                            <div class="option-search disco">
                                <img src="<?php echo wp_get_attachment_image_src( $image2, 'services-min2' )[0] ?>" alt="<?php echo $title2; ?>" title="<?php echo $title2; ?>">
                                <?php echo $label2; ?>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeogroupServicesMin2();
