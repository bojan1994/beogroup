<?php

class vcBeotravelRentACar extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_rentacar_mapping' ) );
        add_shortcode( 'vc_beotravel_rentacar', array( $this, 'vc_beotravel_rentacar_html' ) );
    }
    public function vc_beotravel_rentacar_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Rent a car', 'beogroup' ),
                'base' => 'vc_beotravel_rentacar',
                'description' => __( 'Rent a car', 'beogroup' ),
                'category' => __( 'Beotravel elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'holder' => 'h1',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'textarea',
                        'holder' => 'p',
                        'class' => 'text',
                        'heading' => __( 'Text', 'beotravel' ),
                        'param_name' => 'text',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Text',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beotravel' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                    array(
                        'type' => 'posttypes',
                        'class' => 'services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Rent a car',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_rentacar_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'title' => '',
                    'text' => '',
                    'bgcolor' => '',
                ),
                $atts
            )
        );
        global $post;
        $args = array(
        	'post_type' => 'rent_a_car',
            'posts_per_page' => 20,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="rent-car" style="background-color:<?php echo $bgcolor; ?>">
                <div class="rent-container container">
                    <div class="rent-heading">
                        <h2><?php echo $title; ?></h2>
                        <p><?php echo $text; ?></p>
                    </div>
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="row company">
                                <?php the_post_thumbnail( 'rent-image', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                <div class="rent-details clearfix">
                                    <div class="col-md-6 col-xs-12 col-sm-6 matched rent-info">
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-6 matched rent-img">
                                        <?php echo nl2br( get_post_meta( get_the_ID(), '_usluge_value_key', true ) ); ?>
                                    </div>
                                </div>
                            <?php
                            $contact_url = get_post_meta( get_the_ID(),'_contact_url_value_key6',true );
                            $contact_label = get_post_meta( get_the_ID(),'_contact_value_key6',true );
                            ?>
                            <div class="rent-button">
                                <a class="blue-button" href="<?php echo $contact_url; ?>"><?php echo $contact_label; ?></a>
                            </div>
                        </div>
                    <?php
                endwhile;
                ?>
                </div>
            </div>
            <?php
            wp_reset_postdata();
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeotravelRentACar();
