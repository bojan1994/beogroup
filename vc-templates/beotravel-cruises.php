<?php

class vcBeotravelCruises extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_cruises_mapping' ) );
        add_shortcode( 'vc_beotravel_cruises', array( $this, 'vc_beotravel_cruises_html' ) );
    }
    public function vc_beotravel_cruises_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Beotravel Cruises', 'beotravel' ),
                'base' => 'vc_beotravel_cruises',
                'description' => __( 'Beotravel Cruises', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'slider',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Slider',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_cruises_html( $atts ) {
        $args = array(
        	'post_type' => 'cruises',
            'posts_per_page' => 10,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="cruises">
                <div class="container cruises-container">
                    <div class="row">
                        <?php
                        while( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                            <div class="cruises-info col-xs-12 col-sm-6 col-md-6">
                                <div class="cruise-single">
                                    <div class="cruise-info-img">
                                        <?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    </div>
                                    <div class="cruise-info-bottom">
                                        <div class="cruise-info-text">
                                            <h4><?php the_title(); ?></h4>
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <?php
                                        $cruises_url = get_post_meta( get_the_ID(),'_contact_url_value_key6',true );
                                        $cruises_label = get_post_meta( get_the_ID(),'_contact_value_key6',true );
                                        ?>
                                        <div class="cruises-button">
                                            <a class="blue-button" href="<?php the_permalink(); ?>">Check now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
            <?php
            wp_reset_postdata();
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeotravelCruises();
