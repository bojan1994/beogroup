<?php

class vcBeogroupServicesMin extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beogroup_servicesmin_mapping' ) );
        add_shortcode( 'vc_beogroup_servicesmin', array( $this, 'vc_beogroup_servicesmin_html' ) );
    }
    public function vc_beogroup_servicesmin_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services short', 'beogroup' ),
                'base' => 'vc_beogroup_servicesmin',
                'description' => __( 'Services short', 'beogroup' ),
                'category' => __( 'Beogroup elements', 'beogroup' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'services',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Services short',
                    ),
                )
            )
        );
    }
    public function vc_beogroup_servicesmin_html( $atts ) {
        global $post;
        $args = array(
        	'post_type' => 'services',
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>

                <div class="my-container clearfix">
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        $contact_us_url = get_post_meta( $post->ID, '_contact_url_value_key', true );
                        $image = get_post_meta( $post->ID, '_contact_value_key321', true );
                        ?>
                        <a href="<?php echo $contact_us_url; ?>">
                            <div class="col-lg-3 col-sm-6 col-xs-12 sites">
                                <div class="sites-intro clearfix">
                                    <?php the_post_thumbnail( 'services-min', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                                    <div class="sites-text">
                                        <img src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>

            <?php
        else :
            _e( 'Sorry, no content found', 'beogroup' );
        endif;
    }
}

new vcBeogroupServicesMin();
