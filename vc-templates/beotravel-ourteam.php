<?php

class vcBeotravelOurTeam extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beotravel_ourteam_mapping' ) );
        add_shortcode( 'vc_beotravel_ourteam', array( $this, 'vc_beotravel_ourteam_html' ) );
    }
    public function vc_beotravel_ourteam_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Our team', 'beotravel' ),
                'base' => 'vc_beotravel_ourteam',
                'description' => __( 'Our team', 'beotravel' ),
                'category' => __( 'Beotravel elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'posttypes',
                        'class' => 'our-team',
                        'param_name' => 'image',
                        'value' => '',
                        'group' => 'Our team',
                    ),
                )
            )
        );
    }
    public function vc_beotravel_ourteam_html( $atts ) {
        $html = '';
        global $post;
        $args = array(
        	'post_type' => 'ourteam',
            'posts_per_page' => 12,
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ) :
            ?>
            <div class="beotravel-team-container container">
                <div class="row">
                    <?php
                    while( $query->have_posts() ) :
                        $query->the_post();
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 beotravel-individual individual">
                            <?php the_post_thumbnail( 'beotravel-team', array( 'class' => 'img-responsive', 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?>
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php
        else :
            _e( 'Sorry, no content found.', 'beotravel' );
        endif;
    }
}

new vcBeotravelOurTeam();
