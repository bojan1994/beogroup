<?php $id = hash( 'md5', rand( 10,99 ) . time() ); ?>
<div class="search-modal">
    <div class="search-modal-button">
        <a class="beotravel-carousel-button" href="#" data-toggle="modal" data-target="#<?php echo $id; ?>">More info</a>
    </div>
    <div class="search-form">
        <div class="modal fade" id="<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $id; ?>label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form class="bojanova-forma" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post">
                        <div class="name">
                            <label for="name">Name</label> <br>
                            <input id="name" type="text" name="name"> <br>
                        </div>
                        <div class="email">
                            <label for="email">E-mail</label> <br>
                            <input id="email" type="text" name="email"> <br>
                        </div>
                        <div class="phone">
                            <label for="phone">Phone number</label> <br>
                            <input id="phone" type="text" name="phone"> <br>
                        </div>
                        <div class="msg">
                            <label for="message">Message</label> <br>
                            <textarea id="message" name="message" rows="8" cols="80"></textarea>
                        </div>
                        <input type="hidden" name="property" value="<?php the_title(); ?>">
                        <input type="submit" class="btn btn-primary" name="btn_submit2" value="Send">
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
