<?php

class vcBeoradioServices extends WPBakeryShortCode {
    function __construct() {
        add_action( 'init', array( $this, 'vc_beoradio_services_mapping' ) );
        add_shortcode( 'vc_beoradio_services', array( $this, 'vc_beoradio_services_html' ) );
    }
    public function vc_beoradio_services_mapping() {
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
        vc_map(
            array(
                'name' => __( 'Services', 'beotravel' ),
                'base' => 'vc_beoradio_services',
                'description' => __( 'Services', 'beotravel' ),
                'category' => __( 'Beo Radio elements', 'beotravel' ),
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link1',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 1',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link2',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link3',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 3',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link4',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 4',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link5',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 5',
                    ),
                    array(
                        'type' => 'attach_image',
                        'holder' => 'figure',
                        'class' => 'image',
                        'heading' => __( 'Image', 'beotravel' ),
                        'param_name' => 'image6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'title',
                        'heading' => __( 'Title', 'beotravel' ),
                        'param_name' => 'title6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'subtitle',
                        'heading' => __( 'Subtitle', 'beotravel' ),
                        'param_name' => 'subtitle6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'textfield',
                        'holder' => 'h2',
                        'class' => 'link',
                        'heading' => __( 'Link', 'beotravel' ),
                        'param_name' => 'link6',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Service 6',
                    ),
                    array(
                        'type' => 'colorpicker',
                        'holder' => 'h2',
                        'class' => 'bgcolor',
                        'heading' => __( 'Background color', 'beotravel' ),
                        'param_name' => 'bgcolor',
                        'value' => '',
                        'description' => '',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Backround color',
                    ),
                )
            )
        );
    }
    public function vc_beoradio_services_html( $atts ) {
        extract(
            shortcode_atts(
                array(
                    'bgcolor' => '',
                    'image' => '',
                    'title' => '',
                    'subtitle' => '',
                    'link1' => '',
                    'image2' => '',
                    'title2' => '',
                    'subtitle2' => '',
                    'link2' => '',
                    'image3' => '',
                    'title3' => '',
                    'subtitle3' => '',
                    'link3' => '',
                    'image4' => '',
                    'title4' => '',
                    'subtitle4' => '',
                    'link4' => '',
                    'image5' => '',
                    'title5' => '',
                    'subtitle5' => '',
                    'link5' => '',
                    'image6' => '',
                    'title6' => '',
                    'subtitle6' => '',
                    'link6' => '',
                ),
                $atts
            )
        );
        ?>
        <div class="beotravel-services"  style="background-color:<?php echo $bgcolor; ?>">
            <div class="beotravel-services-container container">
                <div class="beotravel-icons about-site">
                    <a href="<?php echo $link1; ?>"><img src="<?php echo wp_get_attachment_image_src( $image, 'services3' )[0]; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>"></a>
                    <a href="<?php echo $link1; ?>"><h3><?php echo $title; ?></h3></a>
                    <p><?php echo $subtitle; ?></p>
                </div>
                <div class="about-site beotravel-icons">
                    <a href="<?php echo $link2; ?>"><img src="<?php echo wp_get_attachment_image_src( $image2, 'services3' )[0]; ?>" alt="<?php echo $title2; ?>" title="<?php echo $title2; ?>"></a>
                    <a href="<?php echo $link2; ?>"><h3><?php echo $title2; ?></h3></a>
                    <p><?php echo $subtitle2; ?></p>
                </div>
                <div class="about-site beotravel-icons">
                    <a href="<?php echo $link3; ?>"><img src="<?php echo wp_get_attachment_image_src( $image3, 'services3' )[0]; ?>" alt="<?php echo $title3; ?>" title="<?php echo $title3; ?>"></a>
                    <a href="<?php echo $link3; ?>"><h3><?php echo $title3; ?></h3></a>
                    <p><?php echo $subtitle3; ?></p>
                </div>
                <div class="about-site beotravel-icons">
                    <a href="<?php echo $link4; ?>"><img src="<?php echo wp_get_attachment_image_src( $image4, 'services3' )[0]; ?>" alt="<?php echo $title4; ?>" title="<?php echo $title4; ?>"></a>
                    <a href="<?php echo $link4; ?>"><h3><?php echo $title4; ?></h3></a>
                    <p><?php echo $subtitle4; ?></p>
                </div>
                <div class="about-site beotravel-icons">
                    <a href="<?php echo $link5; ?>"><img src="<?php echo wp_get_attachment_image_src( $image5, 'services3' )[0]; ?>" alt="<?php echo $title5; ?>" title="<?php echo $title5; ?>"></a>
                    <a href="<?php echo $link5; ?>"><h3><?php echo $title5; ?></h3></a>
                    <p><?php echo $subtitle5; ?></p>
                </div>
                <div class="about-site beotravel-icons">
                    <a href="<?php echo $link6; ?>"><img src="<?php echo wp_get_attachment_image_src( $image6, 'services3' )[0]; ?>" alt="<?php echo $title6; ?>" title="<?php echo $title6; ?>"></a>
                    <a href="<?php echo $link6; ?>"><h3><?php echo $title6; ?></h3></a>
                    <p><?php echo $subtitle6; ?></p>
                </div>
            </div>
        </div>
        <?php
    }
}

new vcBeoradioServices();
